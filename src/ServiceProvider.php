<?php

namespace OperatingSystems;

use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;
use Kernels\Contracts\Kernel;
use OperatingSystems\Contracts\OperatingSystem;

class ServiceProvider extends IlluminateServiceProvider
{
    public function register()
    {
        app()->singleton(
            OperatingSystem::class,
            fn () => cache()->remember(
                OperatingSystem::class,
                60 * 60,
                fn () => resolve(Kernel::class)->operatingSystem()
            )
        );
    }
}
