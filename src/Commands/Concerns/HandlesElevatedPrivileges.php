<?php

namespace OperatingSystems\Commands\Concerns;

trait HandlesElevatedPrivileges
{
    protected bool $root = false;

    public function runAsRoot(): bool
    {
        // Can we implement the runShellCommand() as a reusable trait?
        // If so, can we then rename this trait to RequiresElevatedPrivileges
        // And have the composition of the shell command string to be executed
        // check for the use of this trait to determine if sudo should be prepended?
        return $this->root;
    }
}
