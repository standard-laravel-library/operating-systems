<?php

namespace OperatingSystems\Commands\Concerns;

use Illuminate\Support\Collection;

trait ForwardsOptionsToShell
{
    protected string $optionPrefix = '--';

    protected string $optionSeparator = '=';

    protected array $forwardableOptions = [];

    final protected function forwardableOptions(): Collection
    {
        return collect($this->options())->filter(
            fn ($value, $key) => in_array($key, $this->forwardableOptions) && $value
        );
    }

    final protected function prepareForwardableOptions(): string
    {
        return $this->forwardableOptions()->map(function ($value, $key) {
            return $this->getDefinition()->getOption($key)->acceptValue()
                ? sprintf('%s%s%s%s', $this->optionPrefix, $key, $this->optionSeparator, $value)
                : sprintf('%s%s', $this->optionPrefix, $key);
        })->join(' ');
    }
}
