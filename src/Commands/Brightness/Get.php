<?php

namespace OperatingSystems\Commands\Brightness;

use Illuminate\Support\Facades\Artisan;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\BrightnessCtl\Console\Commands as BrightnessCtl;

class Get extends Command
{
    protected $signature = 'brightness:get
                            { --j|json : Format the response as JSON. }
    ';

    protected $description = 'Get screen brightness.';

    public function handle()
    {
        // TODO: Figure out which brightness utility is available and use it.
        // TODO: Throw error (Exception?) if not installed? Or Recommend package? Or let OS recommend package?
        Artisan::call(BrightnessCtl\Get::class, ['--json' => $this->option('json')], $this->output);
        if ($this->option('notify')) {
            $this->notify('', Artisan::output());
        }
    }
}
