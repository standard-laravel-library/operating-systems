<?php

namespace OperatingSystems\Commands\Brightness;

use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\BrightnessCtl\Console\Commands as BrightnessCtl;

class Down extends Command
{
    protected $signature = 'brightness:down {amount=8%}';

    protected $description = 'Decrease screen brightness.';

    public function handle()
    {
        // TODO: Figure out which brightness utility is available and use it.
        // TODO: Throw error (Exception?) if not installed? Or Recommend package? Or let OS recommend package?
        $this->call(BrightnessCtl\Down::class, ['amount' => $this->argument('amount')]);
    }
}
