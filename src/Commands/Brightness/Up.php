<?php

namespace OperatingSystems\Commands\Brightness;

use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\BrightnessCtl\Console\Commands as BrightnessCtl;

class Up extends Command
{
    protected $signature = 'brightness:up {amount=10%}';

    protected $description = 'Increase screen brightness.';

    public function handle()
    {
        // TODO: Figure out which brightness utility is available and use it.
        // TODO: Throw error (Exception?) if not installed? Or Recommend package? Or let OS recommend package?
        $this->call(BrightnessCtl\Up::class, ['amount' => $this->argument('amount')]);
    }
}
