<?php

namespace OperatingSystems\Commands\Volume;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\AlsaUtils\Console\Commands\Amixer;

class Get extends Command
{
    protected $signature = 'volume:get
                            { --j|json : Format the response as JSON. }
                            { --N|notify : Display desktop notification. }
    ';

    protected $description = 'Get volume level.';

    public function handle()
    {
        $this->call(Amixer\Get::class, ['--json' => $this->option('json')], $this->output);
        $percentage = Str::of(Artisan::output())->trim("\n");

        if ($this->option('notify')) {
            $this->notify('', $percentage);
        }
    }
}
