<?php

namespace OperatingSystems\Commands\Volume;

use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\AlsaUtils\Console\Commands\Amixer;

class Toggle extends Command
{
    protected $signature = 'volume:toggle
                            { --j|json : Format the response as JSON. }
                            { --N|notify : Display desktop notification. }
    ';

    protected $description = 'Toggle volume.';

    public function handle()
    {
        // TODO: Figure out which volume utility is available and use it.
        // TODO: Throw error (Exception?) if not installed? Or Recommend package? Or let OS recommend package?
        $this->call(Amixer\Toggle::class);
        $this->call(Get::class, ['--json' => $this->option('json'), '--notify' => $this->option('notify')]);
    }
}
