<?php

namespace OperatingSystems\Commands\Volume;

use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\AlsaUtils\Console\Commands\Amixer;

class Mute extends Command
{
    protected $signature = 'volume:mute
                            { --j|json : Format the response as JSON. }
                            { --N|notify : Display desktop notification. }
    ';

    protected $description = 'Mute volume.';

    public function handle()
    {
        // TODO: Figure out which volume utility is available and use it.
        // TODO: Throw error (Exception?) if not installed? Or Recommend package? Or let OS recommend package?
        $this->call(Amixer\Mute::class);
        $this->call(Get::class, ['--json' => $this->option('json'), '--notify' => $this->option('notify')]);
    }
}
