<?php

namespace OperatingSystems\Commands;

use Illuminate\Support\Collection;

class Release extends Command
{
    protected string $binary = 'cat';

    protected $signature = 'release
                            {--a|all : Show all of available information. }
                            {--c|codename : Show the codename. }
                            {--t|title : Show the title. }
                            {--r|release : Show release number. }
                            {--j|json : Format the response as JSON. }
    ';

    protected $description = 'Print operating system release information.';

    protected $columnKeyMap = [
        'ID' => 'title',
        'VERSION_ID' => 'release',
        'VERSION_CODENAME' => 'codename',
    ];

    public function handle()
    {
        $this->option('json') ? $this->asJson() : $this->asTable();
    }

    protected function asTable(): void
    {
        $result = $this->filter();

        $this->table(
            $result->keys()->toArray(),
            [$result->values()->toArray()]
        );
    }

    protected function asJson(): void
    {
        $this->line(
            $this->filter()->toJson()
        );
    }

    protected function raw(): string
    {
        return shell_exec(
            sprintf('%s /etc/os-release', $this->base())
        );
    }

    protected function result(): Collection
    {
        return collect(explode(
            PHP_EOL,
            $this->raw()
        ))->filter()->mapWithKeys(function ($line) {
            [$key, $value] = explode('=', $line);

            return [\Illuminate\Support\Str::replace('=', '', $key) => \Illuminate\Support\Str::replace('"', '', $value)];
        });
    }

    public function filter()
    {
        $options = collect($this->columnKeyMap)->filter(
            fn ($value) => $this->option($value) || $this->option('all'),
        );

        return $this->result()->filter(
            fn ($value, $key) => $options->has($key),
        );
    }
}
