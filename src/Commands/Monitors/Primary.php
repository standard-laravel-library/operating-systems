<?php

namespace OperatingSystems\Commands\Monitors;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Xrandr\Console\Commands\Xrandr;
use Symfony\Component\Console\Output\BufferedOutput;

class Primary extends Command
{
    protected $signature = 'monitors:primary
                            { --N|notify : Display desktop notification. }
    ';

    protected $description = 'Get the primary monitor.';

    protected string $binary = '';

    public function handle()
    {
        // TODO: Why is passing a new BufferedOutput required here but not in \OperatingSystems\Commands\Volume\Get?
        Artisan::call(command: Xrandr\Monitors\Primary::class, outputBuffer: $buffer = new BufferedOutput());

        $this->line(
            $output = (string) Str::of($buffer->fetch())->trim("\n")
        );

        if ($this->option('notify')) {
            $this->notify($output);
        }
    }
}
