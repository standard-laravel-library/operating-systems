<?php

namespace OperatingSystems\Commands;

use OperatingSystems\Packages\Alacritty\Console\Commands\Alacritty;

class Terminal extends Command
{
    protected $signature = 'terminal';

    protected $description = 'Launch default terminal.';

    public function handle()
    {
        $this->call(Alacritty::class);
    }
}
