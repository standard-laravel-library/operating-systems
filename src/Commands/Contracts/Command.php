<?php

namespace OperatingSystems\Commands\Contracts;

interface Command
{
    public function runAsRoot(): bool;

    public function binary(): ?string;

    public function locate(): string;

    public function base(): string;
}
