<?php

namespace OperatingSystems\Commands;

use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\LibNotify\Console\Commands\NotifySend;

class Notify extends Command
{
    protected $signature = 'notify {message}
                            { --c|category : Specifies the notification category. }
                            { --i|icon : Specifies an icon or stock icon to display. }
                            { --t|expire-time : Specifies the timeout in milliseconds. }
                            { --u|urgency= : Specifies the urgency level (low, normal, critical). }
    ';

    protected $description = 'Send desktop notifications to the user via a notification daemon.';

    public function handle()
    {
        // TODO: Figure out which brightness utility is available and use it.
        // TODO: Throw error (Exception?) if not installed? Or Recommend package? Or let OS recommend package?
        $this->call(
            NotifySend::class,
            [
                'message' => $this->argument('message'),
                '--icon' => $this->option('icon'),
                '--urgency' => $this->option('urgency'),
                '--category' => $this->option('category'),
                '--expire-time' => $this->option('expire-time'),
            ]
        );
    }
}
