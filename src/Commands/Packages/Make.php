<?php

namespace OperatingSystems\Commands\Packages;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputOption;

#[AsCommand(name: 'make:package')]
class Make extends GeneratorCommand
{
    protected $name = 'make:package';

    protected static $defaultName = 'make:package';

    protected $description = 'Create a new Package class';

    protected $type = 'Package';

    public function handle()
    {
        parent::handle();

        if ($this->option('apt')) {
            $this->createCandidate('apt');
        }

        if ($this->option('snap')) {
            $this->createCandidate('snap');
        }

        if ($this->option('flatpak')) {
            $this->createCandidate('flatpak');
        }
    }

    protected function createCandidate(string $manager)
    {
        $this->call(
            Candidates\Make::class,
            [
                'name' => $this->getNameInput(),
                '--manager' => $manager,
                '--name' => $this->option($manager),
            ]
        );
    }

    protected function getStub()
    {
        return $this->resolveStubPath('/stubs/package.stub');
    }

    protected function resolveStubPath($stub)
    {
        return __DIR__.$stub;
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\\'.$this->getNameInput();
    }

    protected function rootNamespace()
    {
        return 'OperatingSystems\\Packages\\';
    }

    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return 'operating-systems/Packages/'.str_replace('\\', '/', $name).'.php';
    }

    protected function buildClass($name)
    {
        $stub = str_replace(
            ['{{ name }}'], $this->option('name'), parent::buildClass($name)
        );

        return str_replace(
            ['{{ command }}'], $this->option('command'), $stub
        );
    }

    protected function getOptions()
    {
        return [
            ['name', null, InputOption::VALUE_OPTIONAL, 'The friendly name of the package.', 'Name'],
            ['command', null, InputOption::VALUE_OPTIONAL, 'The shell command.', 'command'],
            ['apt', null, InputOption::VALUE_OPTIONAL, 'The name of the APT package.'],
            ['snap', null, InputOption::VALUE_OPTIONAL, 'The name of the Snap package.'],
            ['flatpak', null, InputOption::VALUE_OPTIONAL, 'The ID of the Flatpak package.'],
        ];
    }
}
