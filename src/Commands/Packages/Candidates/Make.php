<?php

namespace OperatingSystems\Commands\Packages\Candidates;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputOption;

#[AsCommand(name: 'make:candidate')]
class Make extends GeneratorCommand
{
    protected $name = 'make:candidate';

    protected static $defaultName = 'make:candidate';

    protected $description = 'Create a new Candidate for the Package class';

    protected $type = 'Candidate';

    public function handle()
    {
        parent::handle();
    }

    protected function getStub()
    {
        $manager = $this->option('manager');

        return $this->resolveStubPath('/stubs/'.$manager.'.stub');
    }

    protected function resolveStubPath($stub)
    {
        return __DIR__.$stub;
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\\'.$this->getNameInput().'\\Candidates';
    }

    protected function rootNamespace()
    {
        return 'OperatingSystems\\Packages\\';
    }

    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);
        $name = Str::replaceFirst('\\'.$this->getNameInput(), '', $name);

        return 'operating-systems/Packages/'.str_replace('\\', '/', $name).'/'.Str::studly($this->option('manager')).'.php';
    }

    protected function buildClass($name)
    {
        return str_replace(
            ['{{ name }}'], $this->option('name'), parent::buildClass($name)
        );
    }

    protected function getOptions()
    {
        return [
            ['manager', null, InputOption::VALUE_REQUIRED, 'The package manager.', 'apt'],
            ['name', null, InputOption::VALUE_REQUIRED, 'The name of the install candidate.', 'name'],
        ];
    }
}
