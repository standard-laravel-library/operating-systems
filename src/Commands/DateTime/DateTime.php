<?php

namespace OperatingSystems\Commands\DateTime;

use Illuminate\Support\Carbon;
use OperatingSystems\Commands\Command;

class DateTime extends Command
{
    protected $signature = 'datetime
                            { --N|notify : Display desktop notification. }
    ';

    protected $description = 'Get the date & time.';

    protected string $binary = '';

    protected string $package = DateTime::class;

    protected Carbon $now;

    public function handle()
    {
        $this->now = now();

        $this->line(
            $time = $this->now->rawFormat('D, M j, Y H:i:s'),
        );

        if ($this->option('notify')) {
            $this->notify('', $time);
        }
    }
}
