<?php

namespace OperatingSystems\Commands;

use Illuminate\Console\Command as BaseCommand;
use OperatingSystems\Commands\Concerns\ForwardsOptionsToShell;
use OperatingSystems\Commands\Concerns\HandlesElevatedPrivileges;
use OperatingSystems\Kernels\Facades\Kernel;

abstract class Command extends BaseCommand implements Contracts\Command
{
    use ForwardsOptionsToShell;
    use HandlesElevatedPrivileges;

    final public function binary(): ?string
    {
        return $this->binary;
    }

    public function locate(): string
    {
        return Kernel::packageLocator()->find($this->binary);
    }

    public function base(): string
    {
        // Make this toggle between sudo and pkexec depending on if the application
        // is running in CLI mode or not. Using sudo for now as it caches the approval
        // pkexec currently does not cache
        // Do we need one of these XML files https://askubuntu.com/questions/287845/how-to-configure-pkexec/332847#332847?
        return sprintf(
            '%s%s',
            $this->runAsRoot() ? 'sudo ' : '',
            $this->locate()
        );
    }
}
