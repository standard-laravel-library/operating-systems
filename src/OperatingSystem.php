<?php

namespace OperatingSystems;

use Exception;
use Illuminate\Support\Collection;
use OperatingSystems\Contracts\OperatingSystem as OperatingSystemContract;
use OperatingSystems\Packages\Contracts\Manager;

abstract class OperatingSystem implements OperatingSystemContract
{
    protected string $id;

    protected string $name;

    protected string $version;

    protected ?string $codename;

    protected string $defaultPackageManager;

    public function __construct(string $version, ?string $codename)
    {
        $this->version = $version;
        $this->codename = $codename;
    }

    final public function __get($property)
    {
        return match ($property) {
            'id', 'name', 'version', 'codename' => $this->toArray()[$property],
            default => null
        };
    }

    final public function packageManagers(): Collection
    {
        return collect($this->packageManagers ?? [])->map(fn ($class) => resolve($class));
    }

    final public function defaultPackageManager(): Manager
    {
        return new $this->defaultPackageManager;
    }

    final public function homePath($path = '')
    {
        $suffix = ($path ? DIRECTORY_SEPARATOR.ltrim($path, DIRECTORY_SEPARATOR) : $path);

        if ($home = data_get($_SERVER, 'HOME')) {
            return $home.$suffix;
        }

        if ($home = getenv('HOME')) {
            return $home.$suffix;
        }

        if ($home = data_get($_SERVER, 'HOMEDRIVE').data_get($_SERVER, 'HOMEDIR')) {
            return $home.$suffix;
        }

        throw new Exception('Unable to determine home directory');
    }

    final public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'version' => $this->version,
            'codename' => $this->codename,
        ];
    }
}
