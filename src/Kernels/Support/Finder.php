<?php

namespace OperatingSystems\Kernels\Support;

use OperatingSystems\Kernels\Contracts\Kernel;
use OperatingSystems\Kernels\Linux;

class Finder
{
    public static function detect(): ?Kernel
    {
        return match (PHP_OS) {
            'Linux' => new Linux,
            default => null
        };
    }
}
