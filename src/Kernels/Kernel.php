<?php

namespace OperatingSystems\Kernels;

use OperatingSystems\Contracts\OperatingSystem as OperatingSystemContract;
use OperatingSystems\Kernels\Contracts\Kernel as KernelContract;
use OperatingSystems\Packages\Locators\Contracts\Locator as PackageLocatorContract;

class Kernel implements KernelContract
{
    protected string $name;

    protected string $version;

    protected int $major;

    protected int $minor;

    protected int $release;

    protected string $extra;

    public function __construct()
    {
        $this->name = php_uname('s');
        $this->version = php_uname('r');
        $this->major = $this->versionPart('major');
        $this->minor = $this->versionPart('minor');
        $this->release = $this->versionPart('release');
        $this->extra = $this->versionPart('extra');
    }

    final public function operatingSystem(): OperatingSystemContract
    {
        return \OperatingSystems\Support\Finder::detect($this);
    }

    final public function packageLocator(): PackageLocatorContract
    {
        return \OperatingSystems\Packages\Locators\Support\Finder::detect($this);
    }

    final public function __get($property)
    {
        return match ($property) {
            'name', 'version', 'major', 'minor', 'release', 'extra', 'operatingSystem' => $this->toArray()[$property],
            default => null
        };
    }

    private function versionPart($part): int | string
    {
        [$major, $minor, $releaseExtra] = explode('.', $this->version);

        $extras = collect(explode('-', $releaseExtra));

        return match ($part) {
            'major' => intval($major),
            'minor' => intval($minor),
            'release' => intval($extras->pop()),
            'extra' => strval($extras->join('-')),
            default => $this->version
        };
    }

    final public function toArray(): array
    {
        return [
            'name' => $this->name,
            'version' => $this->version,
            'major' => $this->major,
            'minor' => $this->minor,
            'release' => $this->release,
            'extra' => $this->extra,
            'operatingSystem' => $this->operatingSystem()->toArray(),
        ];
    }
}
