<?php

namespace OperatingSystems\Kernels;

use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;
use Kernels\Contracts\Kernel;
use OperatingSystems\Kernels\Support\Finder;

class ServiceProvider extends IlluminateServiceProvider
{
    public function register()
    {
        app()->bind(
            Kernel::class,
            fn () => cache()->remember(
                Kernel::class,
                60 * 60,
                fn () => Finder::detect()
            )
        );
    }
}
