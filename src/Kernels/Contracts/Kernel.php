<?php

namespace OperatingSystems\Kernels\Contracts;

use OperatingSystems\Contracts\OperatingSystem;
use OperatingSystems\Packages\Locators\Contracts\Locator;

interface Kernel
{
    public function packageLocator(): Locator;

    public function operatingSystem(): OperatingSystem;
}
