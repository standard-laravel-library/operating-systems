<?php

namespace OperatingSystems\Kernels\Facades;

use Illuminate\Support\Facades\Facade;
use Kernels\Contracts\Kernel as KernelContract;

class Kernel extends Facade
{
    protected static function getFacadeAccessor()
    {
        return KernelContract::class;
    }
}
