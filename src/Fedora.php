<?php

namespace OperatingSystems;

use OperatingSystems\Packages\Dnf\Dnf;
use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Snap\Snap;

class Fedora extends OperatingSystem
{
    protected string $id = 'fedora';

    protected string $name = 'Fedora';

    protected string $defaultPackageManager = Dnf::class;

    protected array $packageManagers = [
        Dnf::class,
        Flatpak::class,
        Snap::class,
    ];
}
