<?php

namespace OperatingSystems\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use OperatingSystems\Packages\Contracts\Manager;

interface OperatingSystem extends Arrayable
{
    public function defaultPackageManager(): Manager;

    public function packageManagers(): Collection;

    // Once we move this to a normal Laravel app, let's implement this method
    // public function update(): void;
}
