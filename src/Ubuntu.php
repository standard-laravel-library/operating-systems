<?php

namespace OperatingSystems;

use OperatingSystems\Packages\Apt\Apt;
use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Snap\Snap;

class Ubuntu extends OperatingSystem
{
    protected string $id = 'ubuntu';

    protected string $name = 'Ubuntu';

    protected string $defaultPackageManager = Apt::class;

    protected array $packageManagers = [
        Apt::class,
        Flatpak::class,
        Snap::class,
    ];
}
