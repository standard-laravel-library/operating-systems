<?php

namespace OperatingSystems\Packages\PulseAudioVolumeControl\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'pavucontrol';
}
