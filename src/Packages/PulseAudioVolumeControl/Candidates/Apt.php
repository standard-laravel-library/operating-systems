<?php

namespace OperatingSystems\Packages\PulseAudioVolumeControl\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'pavucontrol';
}
