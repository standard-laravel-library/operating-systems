<?php

namespace OperatingSystems\Packages\PulseAudioVolumeControl;

use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Package;

class PulseAudioVolumeControl extends Package
{
    protected string $name = 'PulseAudio Volume Control';

    protected ?string $command = '';

    protected ?string $preferredManager = Flatpak::class;
}
