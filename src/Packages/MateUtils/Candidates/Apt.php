<?php

namespace OperatingSystems\Packages\MateUtils\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'mate-utils';
}
