<?php

namespace OperatingSystems\Packages\MateUtils\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'mate-utils';
}
