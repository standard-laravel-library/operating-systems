<?php

namespace OperatingSystems\Packages\MateUtils;

use OperatingSystems\Packages\Package;

class MateUtils extends Package
{
    protected string $name = 'Mate Utilities';

    protected ?string $command = '';
}
