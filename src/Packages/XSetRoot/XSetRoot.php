<?php

namespace OperatingSystems\Packages\XSetRoot;

use OperatingSystems\Packages\Package;

class XSetRoot extends Package
{
    protected string $name = 'XSetRoot';

    protected ?string $command = 'XSetRoot';
}
