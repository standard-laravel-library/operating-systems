<?php

namespace OperatingSystems\Packages\XSetRoot\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'xsetroot';
}
