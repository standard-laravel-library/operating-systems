<?php

namespace OperatingSystems\Packages\Snap\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'snapd';
}
