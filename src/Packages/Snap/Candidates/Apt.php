<?php

namespace OperatingSystems\Packages\Snap\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'snapd';
}
