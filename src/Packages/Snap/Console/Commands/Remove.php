<?php

namespace OperatingSystems\Packages\Snap\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Snap\Snap;

class Remove extends Command
{
    protected string $binary = 'snap';

    protected bool $root = true;

    protected string $package = Snap::class;

    protected Stringable $raw;

    protected Collection $selections;

    protected Collection $errors;

    protected $signature = 'snap:remove {packages?*}
                            { --no-wait : Do not wait for the operation to finish, just print the change id. }
                            { --revision : Remove only the given revision. }
                            { --purge : Remove the snap without saving a snapshot of its data. }
    ';

    protected $description = 'Snap utility to remove packages.';

    protected array $forwardableOptions = [
        'no-wait',
        'revision',
        'purge',
    ];

    final public function handle()
    {
        $this->init();
        $this->prompt();
        $this->process();
    }

    final protected function init(): void
    {
        $this->selections = collect($this->argument('packages'));
        $this->errors = collect();
    }

    final protected function prompt(): void
    {
        $this->selections->isNotEmpty();
    }

    final protected function process(): void
    {
        $task = 'Removing '.$this->selections->join(', ', ', & ');

        $this->task(
            $task,
            fn () => $this->installSelections()
        );
    }

    final protected function installSelections(): bool
    {
        $this->runShellCommand();
        $this->displayOutput();

        return $this->errors->isEmpty();
    }

    final protected function recordErrors(): void
    {
        $this->errors = $this->raw->explode(PHP_EOL)->filter(
            fn ($line) => Str::of($line)->startsWith('E:')
        );
    }

    final protected function displayOutput(): void
    {
        $this->displayErrors();
    }

    final protected function displayErrors(): void
    {
        if ($this->errors->isNotEmpty()) {
            $this->newLine();
        }

        $this->errors->each(
            fn ($line) => $this->error(Str::of($line)->replace('E:', ''))
        );
    }

    final protected function runShellCommand(): void
    {
        $options = $this->prepareForwardableOptions();

        $command = sprintf(
            '%s remove%s %s 2>&1',
            $this->base(),
            empty($options) ? '' : ' '.$options,
            $this->selections->join(' ')
        );

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        $this->raw = Str::of(shell_exec($command));
        $this->recordErrors();
    }
}
