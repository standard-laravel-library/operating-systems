<?php

namespace OperatingSystems\Packages\Snap\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Contracts\Manager;
use OperatingSystems\Packages\Packages;
use OperatingSystems\Packages\Snap\Snap;

class Installed extends Command
{
    protected string $binary = 'snap';

    protected bool $root = true;

    protected string $package = Snap::class;

    protected Stringable $raw;

    protected Collection $selections;

    protected Collection $errors;

    protected $signature = 'snap:list {packages?*}';

    protected $description = 'Snap utility to list installed packages.';

    final public function handle()
    {
        $this->init();
        $this->prompt();
        $this->process();
    }

    final protected function init(): void
    {
        $this->selections = collect($this->argument('packages'));
        $this->errors = collect();
    }

    final protected function prompt(): void
    {
        $this->selections->isNotEmpty() || $this->displayMenu();
    }

    final protected function displayMenu(): void
    {
        $menu = $this->menu('Packages');

        $this->addPackageCheckbox($menu);

        $install = $menu->addOption('install', 'Install')->open();

        if ($install && empty($this->selections)) {
            $this->prompt();
        }
    }

    final protected function process(): void
    {
        $task = 'Installing '.$this->selections->join(', ', ', & ');

        $this->task(
            $task,
            fn () => $this->installSelections()
        );
    }

    final protected function installSelections(): bool
    {
        $this->runShellCommand();
        $this->displayOutput();

        return $this->errors->isEmpty();
    }

    final protected function recordErrors(): void
    {
        $this->errors = $this->raw->explode(PHP_EOL)->filter(
            fn ($line) => Str::of($line)->startsWith('E:')
        );
    }

    final protected function displayOutput(): void
    {
        $this->displayErrors();
    }

    final protected function displayErrors(): void
    {
        if ($this->errors->isNotEmpty()) {
            $this->newLine();
        }

        $this->errors->each(
            fn ($line) => $this->error(Str::of($line)->replace('E:', ''))
        );
    }

    final public function manager(): Manager
    {
        return resolve($this->package);
    }

    final protected function available()
    {
        return resolve(Packages::class)->candidatesFor($this->manager());
    }

    final protected function addPackageCheckbox($menu): void
    {
        $handler = fn ($menu) => $this->toggleSelection($menu->getSelectedItem()->getText());

        $this->available()->each(
            fn ($candidate) => $menu->addCheckboxItem($candidate->name(), $handler)
        );
    }

    final protected function toggleSelection(string $selection): void
    {
        $this->selections = $this->selections->contains($selection)
            ? $this->selections->reject(fn ($package) => $package === $selection)
            : $this->selections->push($selection);
    }

    final protected function runShellCommand(): void
    {
        $command = sprintf('%s install %s %s 2>&1', $this->base(), $this->options(), $this->selections->join(' '));

        if ($this->getOutput()->isDebug()) {
            'Running shell command: '.$this->info($command);
        }

        $this->raw = Str::of(shell_exec($command));
        $this->recordErrors();
    }
}
