<?php

namespace OperatingSystems\Packages\Snap;

use Illuminate\Support\Facades\Artisan;
use OperatingSystems\Commands\Snap\Install;
use OperatingSystems\Packages\Contracts\Package;
use OperatingSystems\Packages\Manager;

class Snap extends Manager
{
    protected bool $root = true;

    protected string $name = 'Snap';

    protected ?string $command = 'snap';

    protected string $installer = Install::class;

    public function install(Package $package): bool
    {
        // This concept may not be ideal in the end, might not make sense for these classes
        // to have these actions as they require sudo privileges. However, we might also
        // we may also be able to add instructions to add current user to the sudo
        // user list which then would allow these to happen in the application
        $installer = $this->installer();
        $candidate = $package->candidateFor($installer->manager());

        Artisan::call(
            $installer::class,
            $candidate->flags()->put('packages', $candidate->name())->toArray()
        );

        return true;
    }
}
