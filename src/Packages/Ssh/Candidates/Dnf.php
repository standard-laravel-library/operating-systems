<?php

namespace OperatingSystems\Packages\Ssh\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'openssh';
}
