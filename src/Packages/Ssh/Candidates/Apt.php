<?php

namespace OperatingSystems\Packages\Ssh\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'ssh';
}
