<?php

namespace OperatingSystems\Packages\Ssh;

use OperatingSystems\Packages\Package;

class Ssh extends Package
{
    protected string $name = 'SSH';

    protected ?string $command = 'ssh';
}
