<?php

namespace OperatingSystems\Packages\Fish;

use OperatingSystems\Packages\Package;

class Fish extends Package
{
    protected string $name = 'FISH';

    protected ?string $command = 'fish';
}
