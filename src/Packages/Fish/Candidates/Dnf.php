<?php

namespace OperatingSystems\Packages\Fish\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'fish';
}
