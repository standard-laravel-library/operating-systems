<?php

namespace OperatingSystems\Packages\AsciiArtLibrary\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'aalib';
}
