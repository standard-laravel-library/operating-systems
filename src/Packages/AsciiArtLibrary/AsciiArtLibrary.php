<?php

namespace OperatingSystems\Packages\AsciiArtLibrary;

use OperatingSystems\Packages\Package;

class AsciiArtLibrary extends Package
{
    protected string $name = 'ASCII Art Library';

    protected ?string $command = '';
}
