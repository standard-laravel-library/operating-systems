<?php

namespace OperatingSystems\Packages\Candidates;

use OperatingSystems\Packages\Apt\Apt as Manager;

abstract class Apt extends Candidate implements Contracts\Apt
{
    protected string $manager = Manager::class;

    protected ?string $repository;

    final public function repository(): ?string
    {
        return $this->repository;
    }
}
