<?php

namespace OperatingSystems\Packages\Candidates;

use OperatingSystems\Packages\Flatpak\Flatpak as Manager;
use OperatingSystems\Packages\Flatpak\Repositories\Contracts\Repository;

abstract class Flatpak extends Candidate implements Contracts\Flatpak
{
    protected string $manager = Manager::class;

    protected string $repository;

    final public function repository(): Repository
    {
        return resolve($this->repository);
    }
}
