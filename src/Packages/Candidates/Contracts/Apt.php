<?php

namespace OperatingSystems\Packages\Candidates\Contracts;

interface Apt extends Candidate
{
    public function repository(): ?string;
}
