<?php

namespace OperatingSystems\Packages\Candidates\Contracts;

interface Dnf extends Candidate
{
    public function repository(): ?string;
}
