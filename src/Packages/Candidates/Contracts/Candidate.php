<?php

namespace OperatingSystems\Packages\Candidates\Contracts;

use OperatingSystems\Packages\Contracts\Manager;
use OperatingSystems\Packages\Contracts\Package;

interface Candidate
{
    public function name(): string;

    public function manager(): Manager;

    public function package(): Package;
}
