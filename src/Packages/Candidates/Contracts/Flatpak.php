<?php

namespace OperatingSystems\Packages\Candidates\Contracts;

use OperatingSystems\Packages\Flatpak\Repositories\Contracts\Repository;

interface Flatpak extends Candidate
{
    public function repository(): Repository;
}
