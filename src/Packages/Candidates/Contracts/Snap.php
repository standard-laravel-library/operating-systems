<?php

namespace OperatingSystems\Packages\Candidates\Contracts;

use Illuminate\Support\Collection;

interface Snap extends Candidate
{
    public function flags(): Collection;
}
