<?php

namespace OperatingSystems\Packages\Candidates;

use OperatingSystems\Packages\Dnf\Dnf as Manager;

abstract class Dnf extends Candidate implements Contracts\Dnf
{
    protected string $manager = Manager::class;

    protected ?string $repository;

    // TODO: Repositories should probably be high level concepts represented by classes
    final public function repository(): ?string
    {
        return $this->repository;
    }
}
