<?php

namespace OperatingSystems\Packages\Candidates;

use Illuminate\Support\Str;
use OperatingSystems\Packages\Contracts\Manager;
use OperatingSystems\Packages\Contracts\Package;

abstract class Candidate implements Contracts\Candidate
{
    protected string $name;

    protected string $manager;

    protected Package $package;

    final public function name(): string
    {
        return $this->name;
    }

    final public function manager(): Manager
    {
        return resolve($this->manager);
    }

    final public function package(): Package
    {
        $namespace = Str::of((new \ReflectionClass($this::class))->getNamespaceName())->replace('\\Candidates', '');

        $package = $namespace->explode('OperatingSystems\\Packages\\')->filter()->first();

        return resolve($namespace.'\\'.$package);
    }
}
