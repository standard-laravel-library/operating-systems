<?php

namespace OperatingSystems\Packages\Candidates;

use Illuminate\Support\Collection;
use OperatingSystems\Packages\Snap\Snap as Manager;

abstract class Snap extends Candidate implements Contracts\Snap
{
    protected string $manager = Manager::class;

    protected array $flags = [];

    final public function flags(): Collection
    {
        return collect($this->flags);
    }
}
