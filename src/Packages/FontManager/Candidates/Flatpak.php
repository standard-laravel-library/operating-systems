<?php

namespace OperatingSystems\Packages\FontManager\Candidates;

use OperatingSystems\Packages\Candidates;
use OperatingSystems\Packages\Flatpak\Repositories\Contracts\Flathub;

class Flatpak extends Candidates\Flatpak
{
    protected string $name = 'org.gnome.FontManager';

    protected string $repository = Flathub::class;
}
