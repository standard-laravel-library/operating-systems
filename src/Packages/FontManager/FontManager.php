<?php

namespace OperatingSystems\Packages\FontManager;

use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Package;

class FontManager extends Package
{
    protected string $name = 'Font Manager';

    protected ?string $command = '';

    protected ?string $preferredManager = Flatpak::class;
}
