<?php

namespace OperatingSystems\Packages\XSettingsD\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'xsettingsd';
}
