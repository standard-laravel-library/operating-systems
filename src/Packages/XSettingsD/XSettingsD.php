<?php

namespace OperatingSystems\Packages\XSettingsD;

use OperatingSystems\Packages\Package;

class XSettingsD extends Package
{
    protected string $name = 'XSettingsD';

    protected ?string $command = 'xsettingsd';
}
