<?php

namespace OperatingSystems\Packages\Cava\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'cava';
}
