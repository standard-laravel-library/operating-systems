<?php

namespace OperatingSystems\Packages\Cava\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'cava';
}
