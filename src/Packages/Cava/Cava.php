<?php

namespace OperatingSystems\Packages\Cava;

use OperatingSystems\Packages\Package;

class Cava extends Package
{
    protected string $name = 'Cava';

    protected ?string $command = 'cava';
}
