<?php

namespace OperatingSystems\Packages\AlsaUtils;

use OperatingSystems\Packages\Dnf\Dnf;
use OperatingSystems\Packages\Package;

class AlsaUtils extends Package
{
    protected string $name = 'Alsa Utilities';

    protected ?string $command = '';

    protected ?string $preferredManager = Dnf::class;
}
