<?php

namespace OperatingSystems\Packages\AlsaUtils\Console\Commands\Amixer;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;

class Get extends Command
{
    protected $signature = 'amixer:get {device=Master}
                            {--j|json : Format the response as JSON. }
    ';

    protected $description = 'Get device volume.';

    protected string $binary = 'amixer';

    protected string $package = '';

    protected Stringable $raw;

    protected Collection $errors;

    public function handle()
    {
        $this->runShellCommand();

        $this->option('json') ? $this->asJson() : $this->asString();
    }

    private function asJson(): void
    {
        $this->line(
            json_encode($this->result())
        );
    }

    private function asString(): void
    {
        $this->info(
            $this->result()
        );
    }

    private function result(): Stringable
    {
        if ($this->raw->contains('[off]')) {
            return Str::of('Muted');
        }

        return $this->raw->match('/[0-9]+%/');
    }

    private function runShellCommand()
    {
        $command = sprintf('amixer get %s', $this->argument('device'));

        $this->raw = Str::of(
            shell_exec($command)
        );
    }
}
