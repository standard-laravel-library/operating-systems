<?php

namespace OperatingSystems\Packages\AlsaUtils\Console\Commands\Amixer;

use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;

class Toggle extends Command
{
    protected string $binary = 'amixer';

    protected string $package = '';

    protected Stringable $raw;

    protected Collection $errors;

    protected $signature = 'amixer:toggle {device=Master}';

    protected $description = 'Mute device volume.';

    public function handle()
    {
        $command = sprintf('amixer set %s toggle', $this->argument('device'));

        shell_exec($command);
    }
}
