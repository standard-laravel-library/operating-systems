<?php

namespace OperatingSystems\Packages\AlsaUtils\Console\Commands\Amixer;

use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;

class Up extends Command
{
    protected string $binary = 'amixer';

    protected string $package = '';

    protected Stringable $raw;

    protected Collection $errors;

    protected $signature = 'amixer:up {amount=2%} {device=Master}';

    protected $description = 'Increase device volume.';

    public function handle()
    {
        $this->call(Unmute::class);
        $command = sprintf('amixer set %s %s+', $this->argument('device'), $this->argument('amount'));

        shell_exec($command);
    }
}
