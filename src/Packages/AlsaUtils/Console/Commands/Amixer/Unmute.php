<?php

namespace OperatingSystems\Packages\AlsaUtils\Console\Commands\Amixer;

use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;

class Unmute extends Command
{
    protected string $binary = 'amixer';

    protected string $package = '';

    protected Stringable $raw;

    protected Collection $errors;

    protected $signature = 'amixer:unmute {device=Master}';

    protected $description = 'Unmute device volume.';

    public function handle()
    {
        $command = sprintf('amixer set %s unmute', $this->argument('device'));

        shell_exec($command);
    }
}
