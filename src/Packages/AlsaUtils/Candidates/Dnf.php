<?php

namespace OperatingSystems\Packages\AlsaUtils\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'alsa-utils';
}
