<?php

namespace OperatingSystems\Packages\Feh\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'feh';
}
