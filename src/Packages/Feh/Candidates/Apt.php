<?php

namespace OperatingSystems\Packages\Feh\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'feh';
}
