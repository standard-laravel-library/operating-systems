<?php

namespace OperatingSystems\Packages\Feh;

use OperatingSystems\Packages\Package;

class Feh extends Package
{
    protected string $name = 'Feh';

    protected ?string $command = 'feh';
}
