<?php

namespace OperatingSystems\Packages\Unimatrix\Candidates;

use OperatingSystems\Packages\Candidates;

class Snap extends Candidates\Snap
{
    protected string $name = 'unimatrix';
}
