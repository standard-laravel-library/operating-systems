<?php

namespace OperatingSystems\Packages\Unimatrix;

use OperatingSystems\Packages\Package;
use OperatingSystems\Packages\Snap\Snap;

class Unimatrix extends Package
{
    protected string $name = 'Unimatrix';

    protected ?string $command = 'unimatrix';

    protected ?string $preferredManager = Snap::class;
}
