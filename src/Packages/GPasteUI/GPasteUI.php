<?php

namespace OperatingSystems\Packages\GPasteUI;

use OperatingSystems\Packages\Package;

class GPasteUI extends Package
{
    protected string $name = 'GPaste UI';

    protected ?string $command = '';
}
