<?php

namespace OperatingSystems\Packages\GPasteUI\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'gpaste-ui';
}
