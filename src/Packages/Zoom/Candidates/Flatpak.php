<?php

namespace OperatingSystems\Packages\Zoom\Candidates;

use OperatingSystems\Packages\Candidates;
use OperatingSystems\Packages\Flatpak\Repositories\Contracts\Flathub;

class Flatpak extends Candidates\Flatpak
{
    protected string $name = 'us.zoom.Zoom';

    protected string $repository = Flathub::class;
}
