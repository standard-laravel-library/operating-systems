<?php

namespace OperatingSystems\Packages\Zoom;

use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Package;

class Zoom extends Package
{
    protected string $name = 'Zoom';

    protected ?string $command = '';

    protected ?string $preferredManager = Flatpak::class;
}
