<?php

namespace OperatingSystems\Packages\Cheese;

use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Package;

class Cheese extends Package
{
    protected string $name = 'Cheese';

    protected ?string $command = 'command';

    protected ?string $preferredManager = Flatpak::class;
}
