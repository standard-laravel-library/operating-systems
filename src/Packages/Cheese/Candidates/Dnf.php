<?php

namespace OperatingSystems\Packages\Cheese\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'cheese';
}
