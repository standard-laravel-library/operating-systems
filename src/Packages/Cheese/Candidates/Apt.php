<?php

namespace OperatingSystems\Packages\Cheese\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'cheese';
}
