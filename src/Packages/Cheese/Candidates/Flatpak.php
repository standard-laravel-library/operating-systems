<?php

namespace OperatingSystems\Packages\Cheese\Candidates;

use OperatingSystems\Packages\Candidates;
use OperatingSystems\Packages\Flatpak\Repositories\Contracts\Flathub;

class Flatpak extends Candidates\Flatpak
{
    protected string $name = 'org.gnome.Cheese';

    protected string $repository = Flathub::class;
}
