<?php

namespace OperatingSystems\Packages\FontAwesome;

use OperatingSystems\Packages\Package;

class FontAwesome extends Package
{
    protected string $name = 'FontAwesome';

    protected ?string $command = '';
}
