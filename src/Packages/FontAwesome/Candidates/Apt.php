<?php

namespace OperatingSystems\Packages\FontAwesome\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'fonts-font-awesome';
}
