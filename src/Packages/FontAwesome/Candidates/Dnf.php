<?php

namespace OperatingSystems\Packages\FontAwesome\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'fontawesome-fonts';
}
