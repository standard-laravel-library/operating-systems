<?php

namespace OperatingSystems\Packages\Peek;

use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Package;

class Peek extends Package
{
    protected string $name = 'Peek';

    protected ?string $command = '';

    protected ?string $preferredManager = Flatpak::class;
}
