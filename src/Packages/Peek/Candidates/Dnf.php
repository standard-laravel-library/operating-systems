<?php

namespace OperatingSystems\Packages\Peek\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'peek';
}
