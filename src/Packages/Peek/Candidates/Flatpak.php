<?php

namespace OperatingSystems\Packages\Peek\Candidates;

use OperatingSystems\Packages\Candidates;
use OperatingSystems\Packages\Flatpak\Repositories\Contracts\Flathub;

class Flatpak extends Candidates\Flatpak
{
    protected string $name = 'com.uploadedlobster.peek';

    protected string $repository = Flathub::class;
}
