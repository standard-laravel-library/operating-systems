<?php

namespace OperatingSystems\Packages\Curl\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'curl';
}
