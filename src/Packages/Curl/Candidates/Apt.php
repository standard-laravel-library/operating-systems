<?php

namespace OperatingSystems\Packages\Curl\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'curl';
}
