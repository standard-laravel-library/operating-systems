<?php

namespace OperatingSystems\Packages\Curl;

use OperatingSystems\Packages\Package;

class Curl extends Package
{
    protected string $name = 'cURL';

    protected ?string $command = 'curl';
}
