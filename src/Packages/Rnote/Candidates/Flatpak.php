<?php

namespace OperatingSystems\Packages\Rnote\Candidates;

use OperatingSystems\Packages\Candidates;
use OperatingSystems\Packages\Flatpak\Repositories\Contracts\Flathub;

class Flatpak extends Candidates\Flatpak
{
    protected string $name = 'com.github.flxzt.rnote';

    protected string $repository = Flathub::class;
}
