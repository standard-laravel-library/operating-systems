<?php

namespace OperatingSystems\Packages\Rnote;

use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Package;

class Rnote extends Package
{
    protected string $name = 'RNote';

    protected ?string $command = 'command';

    protected ?string $preferredManager = Flatpak::class;
}
