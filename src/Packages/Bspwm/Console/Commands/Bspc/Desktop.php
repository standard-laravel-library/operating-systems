<?php

namespace OperatingSystems\Packages\Bspwm\Console\Commands\Bspc;

use Illuminate\Support\Str;
use OperatingSystems\Commands\Command;

class Desktop extends Command
{
    protected $signature = 'bspc:desktop {desktop}
                            { --f|focus : Focus the selected or given desktop. }
    ';

    protected $description = 'Interact with binary space partitioning window manager desktops.';

    protected string $binary = 'bspc';

    protected array $forwardableOptions = [
        'focus',
    ];

    public function handle()
    {
        $this->runShellCommand();
    }

    protected function runShellCommand(): void
    {
        $options = $this->prepareForwardableOptions();

        $command = sprintf(
            '%s desktop %s %s 2>&1',
            $this->base(),
            $this->argument('desktop'),
            empty($options) ? '' : ' '.$options,
        );

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        $this->raw = Str::of(shell_exec($command));
    }

    protected function displayOutput(): void
    {
        collect($this->raw->explode("\n"))->filter()->each(
            fn (string $line) => $this->line($line)
        );
    }
}
