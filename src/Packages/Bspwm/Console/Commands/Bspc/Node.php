<?php

namespace OperatingSystems\Packages\Bspwm\Console\Commands\Bspc;

use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Sockets\Socket;
use OperatingSystems\Commands\Command;

class Node extends Command
{
    protected $signature = 'bspc:node {node?}
                            { --s|swap : Swap the selected node with the given node. }
                            { --f|focus : Focus the selected or given desktop. }
                            { --d|to-desktop= : Send the selected node to the given desktop. }
                            { --t|state= : Set the state of the selected window. }
                            { --follow : If passed, the focused node will stay focused. }
                            { --R|rotate= : Rotate the tree rooted at the selected node. }
                            { --c|close : Close the windows rooted at the selected node. }
                            { --k|kill : Kill the windows rooted at selected node. }
                            { --shell : Execute with the shell instead of the socket communication. }
    ';

    protected $description = 'Interact with binary space partitioning window manager nodes.';

    protected string $binary = 'bspc';

    protected array $forwardableOptions = [
        'swap',
        'focus',
        'to-desktop',
        'state',
        'follow',
        'rotate',
        'kill',
        'close',
    ];

    protected string $optionSeparator = ' ';

    public function handle()
    {
        $this->option('shell') ? $this->runShellCommand() : $this->sendSocketMessage();
    }

    protected function sendSocketMessage()
    {
        $file = '/tmp/bspwm_0_0-socket';
        $socket = (new Socket)->unix()->stream()->protocol(0)->address($file);

        $socket->connect()->write(
            $this->buildSocketMessage()
        );

        $this->info($socket->read());

        $socket->close();
    }

    protected function buildShellCommand(): Stringable
    {
        $options = $this->prepareForwardableOptions();

        return Str::of(
            sprintf(
                '%s node%s%s',
                $this->base(),
                empty($options) ? '' : ' '.$options,
                $this->argument('node') ? ' '.$this->argument('node') : '',
            )
        );
    }

    protected function buildSocketMessage(): Stringable
    {
        return $this->buildShellCommand()->replace($this->base() . ' ', '')->replace(' ', "\0")->append("\0");
    }

    protected function runShellCommand(): void
    {
        $command = $this->buildShellCommand() . ' 2>&1';

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        $this->raw = Str::of(shell_exec($command));
    }
}
