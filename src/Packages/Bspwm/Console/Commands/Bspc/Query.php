<?php

namespace OperatingSystems\Packages\Bspwm\Console\Commands\Bspc;

use Illuminate\Support\Str;
use OperatingSystems\Commands\Command;

class Query extends Command
{
    protected $signature = 'bspc:query
                            { --N|nodes : List the IDs of the matching nodes. }
                            { --D|desktops : List the IDs of the matching desktops. }
                            { --M|monitors : List the IDs of the matching monitors. }
                            { --node= : Constraint matches to the selected node. }
                            { --desktop= : Constraint matches to the selected desktop. }
                            { --monitor= : Constraint matches to the selected monitor. }
                            { --names= : Print names instead of IDs. }
    ';

    protected $description = 'Query binary space partitioning window manager.';

    protected string $binary = 'bspc';

    protected array $forwardableOptions = [
        'nodes',
        'desktops',
        'monitors',
        'monitor',
        'desktop',
        'node',
        'names',
    ];

    public function handle()
    {
        $this->runShellCommand();
        $this->displayOutput();
    }

    protected function runShellCommand(): void
    {
        $options = $this->prepareForwardableOptions();

        $command = sprintf(
            '%s query %s 2>&1',
            $this->base(),
            empty($options) ? '' : ' '.$options,
        );

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        $this->raw = Str::of(shell_exec($command));
    }

    protected function displayOutput(): void
    {
        collect($this->raw->explode("\n"))->filter()->each(
            fn (string $line) => $this->line($line)
        );
    }
}
