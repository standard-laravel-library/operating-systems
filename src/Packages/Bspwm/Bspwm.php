<?php

namespace OperatingSystems\Packages\Bspwm;

use OperatingSystems\Packages\Package;

class Bspwm extends Package
{
    protected string $name = 'BSPWM';

    protected ?string $command = 'bspwm';
}
