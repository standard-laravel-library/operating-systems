<?php

namespace OperatingSystems\Packages\Bspwm\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'bspwm';
}
