<?php

namespace OperatingSystems\Packages\Bspwm\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'bspwm';
}
