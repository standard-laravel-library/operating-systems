<?php

namespace OperatingSystems\Packages\BrightnessCtl\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;

class Down extends Command
{
    protected string $binary = 'brightnessctl';

    protected string $package = '';

    protected Stringable $raw;

    protected Collection $errors;

    protected $signature = 'brightnessctl:down {amount=8%}';

    protected $description = 'Decrease screen brightness.';

    public function handle()
    {
        $command = sprintf('brightnessctl set %s-', $this->argument('amount'));

        shell_exec($command);
    }
}
