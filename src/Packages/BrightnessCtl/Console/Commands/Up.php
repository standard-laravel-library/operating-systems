<?php

namespace OperatingSystems\Packages\BrightnessCtl\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;

class Up extends Command
{
    protected string $binary = 'brightnessctl';

    protected string $package = '';

    protected Stringable $raw;

    protected Collection $errors;

    protected $signature = 'brightnessctl:up {amount=10%}';

    protected $description = 'Increase screen brightness.';

    public function handle()
    {
        $command = sprintf('brightnessctl set +%s', $this->argument('amount'));

        shell_exec($command);
    }
}
