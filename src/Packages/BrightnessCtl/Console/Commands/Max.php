<?php

namespace OperatingSystems\Packages\BrightnessCtl\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;

class Max extends Command
{
    protected string $binary = 'brightnessctl';

    protected string $package = '';

    protected Stringable $raw;

    protected Collection $errors;

    protected $signature = 'brightnessctl:max
                            {--j|json : Format the response as JSON. }
    ';

    protected $description = 'Get maximum screen brightness.';

    public function handle()
    {
        $command = sprintf('brightnessctl max');

        $this->raw = Str::of(shell_exec($command))->trim("\n");

        $this->option('json') ? $this->line(json_encode($this->raw)) : $this->info($this->raw);
    }
}
