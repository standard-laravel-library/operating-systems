<?php

namespace OperatingSystems\Packages\BrightnessCtl\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;
use SebastianBergmann\CodeCoverage\Util\Percentage;

class Get extends Command
{
    protected string $binary = 'brightnessctl';

    protected string $package = '';

    protected Stringable $raw;

    protected Collection $errors;

    protected $signature = 'brightnessctl:get
                            {--j|json : Format the response as JSON. }
                            {--r|raw : Get the raw brightness. }
    ';

    protected $description = 'Get screen brightness.';

    public function handle()
    {
        Artisan::call(Max::class, ['--json' => $this->option('json')]);
        $max = json_decode(Str::of(Artisan::output())->trim("\n"));

        $command = sprintf('brightnessctl get');

        $this->raw = Str::of(shell_exec($command))->trim('\n');

        $percent = Percentage::fromFractionAndTotal((float) (string) $this->raw, (float) (string) $max);

        $this->option('json') ? $this->line(json_encode($percent->asFloat())) : $this->info($percent->asString());
    }
}
