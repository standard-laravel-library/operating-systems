<?php

namespace OperatingSystems\Packages\BrightnessCtl;

use OperatingSystems\Packages\Package;

class BrightnessCtl extends Package
{
    protected string $name = 'BrightnessCTL';

    protected ?string $command = 'brightnessctl';
}
