<?php

namespace OperatingSystems\Packages\Lxappearance;

use OperatingSystems\Packages\Package;

class Lxappearance extends Package
{
    protected string $name = 'LXDE GTK+ theme switcher';
}
