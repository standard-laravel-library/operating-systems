<?php

namespace OperatingSystems\Packages\Lxappearance\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'lxappearance';
}
