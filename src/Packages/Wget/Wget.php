<?php

namespace OperatingSystems\Packages\Wget;

use OperatingSystems\Packages\Package;

class Wget extends Package
{
    protected string $name = 'Wget';

    protected ?string $command = 'wget';
}
