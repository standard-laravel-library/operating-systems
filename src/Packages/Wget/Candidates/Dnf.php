<?php

namespace OperatingSystems\Packages\Wget\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'wget';
}
