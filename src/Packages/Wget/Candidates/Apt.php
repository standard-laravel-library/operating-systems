<?php

namespace OperatingSystems\Packages\Wget\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'wget';
}
