<?php

namespace OperatingSystems\Packages\Polybar;

use OperatingSystems\Packages\Package;

class Polybar extends Package
{
    protected string $name = 'Polybar';

    protected ?string $command = 'polybar';
}
