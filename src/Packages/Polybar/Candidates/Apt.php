<?php

namespace OperatingSystems\Packages\Polybar\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'polybar';
}
