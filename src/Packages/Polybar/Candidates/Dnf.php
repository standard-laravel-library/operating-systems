<?php

namespace OperatingSystems\Packages\Polybar\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'polybar';
}
