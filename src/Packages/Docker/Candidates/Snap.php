<?php

namespace OperatingSystems\Packages\Docker\Candidates;

use OperatingSystems\Packages\Candidates;

class Snap extends Candidates\Snap
{
    protected string $name = 'docker';
}
