<?php

namespace OperatingSystems\Packages\Docker\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'docker';
}
