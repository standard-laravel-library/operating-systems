<?php

namespace OperatingSystems\Packages\Docker;

use OperatingSystems\Packages\Package;
use OperatingSystems\Packages\Snap\Snap;

class Docker extends Package
{
    protected string $name = 'Docker';

    protected ?string $command = 'docker';

    protected ?string $preferredManager = Snap::class;
}
