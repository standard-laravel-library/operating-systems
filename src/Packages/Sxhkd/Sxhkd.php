<?php

namespace OperatingSystems\Packages\Sxhkd;

use OperatingSystems\Packages\Package;

class Sxhkd extends Package
{
    protected string $name = 'SXHKD';

    protected ?string $command = 'sxhkd';
}
