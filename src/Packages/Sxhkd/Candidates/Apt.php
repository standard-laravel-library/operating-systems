<?php

namespace OperatingSystems\Packages\Sxhkd\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'sxhkd';
}
