<?php

namespace OperatingSystems\Packages\Sxhkd\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'sxhkd';
}
