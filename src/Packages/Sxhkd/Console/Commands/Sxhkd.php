<?php

namespace OperatingSystems\Packages\Sxhkd\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;

class Sxhkd extends Command
{
    protected string $binary = 'sxhkd';

    protected string $package = '';

    protected Stringable $raw;

    protected $signature = 'sxhkd
                            { --a|abort-key: Name of the keysym used for aborting chord chains. }
                            { --m|count: Handle the first COUNT mapping notify events. }
                            { --K|killall: Kill all running processes. }
    ';

    protected $description = 'Simple X hotkey daemon.';

    public function handle()
    {

    }
}
