<?php

namespace OperatingSystems\Packages\Slack;

use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Package;

class Slack extends Package
{
    protected string $name = 'Slack';

    protected ?string $command = '';

    protected ?string $preferredManager = Flatpak::class;
}
