<?php

namespace OperatingSystems\Packages\Slack\Candidates;

use OperatingSystems\Packages\Candidates;
use OperatingSystems\Packages\Flatpak\Repositories\Contracts\Flathub;

class Flatpak extends Candidates\Flatpak
{
    protected string $name = 'com.slack.Slack';

    protected string $repository = Flathub::class;
}
