<?php

namespace OperatingSystems\Packages\PolkitGnome;

use OperatingSystems\Packages\Package;

// TODO: What is the best way to start this automatically for tiling managers?
// TODO: Does this belong in `php desktop startup`?
// TODO: How do we execute `php desktop startup` regardless of the DM/WM? Desktop file in an autostart directory?
class PolkitGnome extends Package
{
    protected string $name = 'PolkitGnome';

    // TODO: This is the path of the command in Fedora, Ubuntu and other distributions may be different how do we handle these types of situations?
    // TODO: Should this have a dedicated command that figures this stuff out for us?
    protected ?string $command = '/usr/libexec/polkit-gnome-authentication-agent-1';
}
