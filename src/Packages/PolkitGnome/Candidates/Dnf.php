<?php

namespace OperatingSystems\Packages\PolkitGnome\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'polkit-gnome';
}
