<?php

namespace OperatingSystems\Packages\BreezeCursorTheme;

use OperatingSystems\Packages\Package;

class BreezeCursorTheme extends Package
{
    protected string $name = 'Breeze Cursor Theme';
}
