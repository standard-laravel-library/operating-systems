<?php

namespace OperatingSystems\Packages\BreezeCursorTheme\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'breeze-cursor-theme';
}
