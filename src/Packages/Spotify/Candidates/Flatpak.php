<?php

namespace OperatingSystems\Packages\Spotify\Candidates;

use OperatingSystems\Packages\Candidates;
use OperatingSystems\Packages\Flatpak\Repositories\Contracts\Flathub;

class Flatpak extends Candidates\Flatpak
{
    protected string $name = 'com.spotify.Client';

    protected string $repository = Flathub::class;
}
