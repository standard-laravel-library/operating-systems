<?php

namespace OperatingSystems\Packages\Spotify;

use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Package;

class Spotify extends Package
{
    protected string $name = 'Spotify';

    protected ?string $command = '';

    protected ?string $preferredManager = Flatpak::class;
}
