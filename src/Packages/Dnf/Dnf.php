<?php

namespace OperatingSystems\Packages\Dnf;

use Illuminate\Support\Facades\Artisan;
use OperatingSystems\Commands\Dnf\Install;
use OperatingSystems\Packages\Contracts\Package;
use OperatingSystems\Packages\Manager;

class Dnf extends Manager
{
    protected bool $root = true;

    protected ?string $command = 'dnf';

    protected string $installer = Install::class;

    public function install(Package $package): bool
    {
        // This concept may not be ideal in the end, might not make sense for these classes
        // to have these actions as they require sudo privileges. However, we might also
        // we may also be able to add instructions to add current user to the sudo
        // user list which then would allow these to happen in the application
        $installer = $this->installer();
        $candidate = $package->candidateFor($installer->manager());

        Artisan::call(
            $installer::class,
            ['packages' => $candidate->name(), '-y' => true]
        );

        return true;
    }
}
