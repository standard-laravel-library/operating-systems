<?php

namespace OperatingSystems\Packages\Dnf\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Contracts\Manager;
use OperatingSystems\Packages\Dnf\Dnf;
use OperatingSystems\Packages\Packages;

class Install extends Command
{
    protected string $binary = 'dnf';

    protected bool $root = true;

    protected string $package = Dnf::class;

    protected Stringable $raw;

    protected Collection $selections;

    protected Collection $errors;

    protected $signature = 'dnf:install {packages?*}
                            { --y|assumeyes : Automatic yes to prompts }
    ';

    protected $description = 'DNF package installation utility.';

    protected array $forwardableOptions = [
        'assumeyes',
    ];

    final public function handle()
    {
        $this->init();
        $this->prompt();
        $this->process();
    }

    final protected function init(): void
    {
        $this->selections = collect($this->argument('packages'));
        $this->errors = collect();
    }

    final protected function prompt(): void
    {
        $this->selections->isNotEmpty();
    }

    final protected function process(): void
    {
        $task = 'Install '.$this->selections->join(', ', ', & ');

        $this->task(
            $task,
            fn () => $this->installSelections()
        );
    }

    final protected function installSelections(): bool
    {
        $this->runShellCommand();
        $this->displayOutput();

        return $this->errors->isEmpty();
    }

    final protected function recordErrors(): void
    {
        $this->errors = $this->raw->explode(PHP_EOL)->filter(
            fn ($line) => Str::of($line)->startsWith('E:')
        );
    }

    final protected function displayOutput(): void
    {
        $this->displayErrors();
    }

    final protected function displayErrors(): void
    {
        if ($this->errors->isNotEmpty()) {
            $this->newLine();
        }

        $this->errors->each(
            fn ($line) => $this->error(Str::of($line)->replace('E:', ''))
        );
    }

    final public function manager(): Manager
    {
        return resolve($this->package);
    }

    final protected function available()
    {
        return resolve(Packages::class)->candidatesFor($this->manager());
    }

    final protected function addPackageCheckbox($menu): void
    {
        $handler = fn ($menu) => $this->toggleSelection($menu->getSelectedItem()->getText());

        $this->available()->each(
            fn ($candidate) => $menu->addCheckboxItem($candidate->name(), $handler)
        );
    }

    final protected function toggleSelection(string $selection): void
    {
        $this->selections = $this->selections->contains($selection)
            ? $this->selections->reject(fn ($package) => $package === $selection)
            : $this->selections->push($selection);
    }

    final protected function runShellCommand(): void
    {
        $options = $this->prepareForwardableOptions();

        $command = sprintf(
            '%s install %s %s 2>&1',
            $this->base(),
            empty($options) ? '' : ' '.$options,
            $this->selections->join(' ')
        );

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        $this->raw = Str::of(shell_exec($command));
        $this->recordErrors();
    }
}
