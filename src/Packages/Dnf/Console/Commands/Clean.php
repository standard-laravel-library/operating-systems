<?php

namespace OperatingSystems\Packages\Dnf\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Managers;

class Clean extends Command
{
    protected string $binary = 'dnf';

    protected bool $root = true;

    protected string $package = Managers\Dnf::class;

    protected Stringable $raw;

    protected Collection $selections;

    protected Collection $errors;

    protected $signature = 'dnf:clean {type=all}';

    protected $description = 'DNF utility to remove cached data.';

    protected array $forwardableOptions = [
        'yes',
    ];

    final public function handle()
    {
        $this->process();
    }

    final protected function process(): void
    {
        $task = 'Clean DNF cache';

        $this->task(
            $task,
            fn () => $this->runShellCommand()
        );
    }

    final protected function runShellCommand(): void
    {
        $command = sprintf('%s clean %s %s 2>&1', $this->base(), $this->prepareForwardableOptions(), $this->argument('type'));

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            'Running shell command: '.$this->info($command);
        }

        $this->raw = Str::of(shell_exec($command));
    }
}
