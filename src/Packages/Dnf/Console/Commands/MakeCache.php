<?php

namespace OperatingSystems\Packages\Dnf\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Managers;

class MakeCache extends Command
{
    protected string $binary = 'dnf';

    protected bool $root = true;

    protected string $package = Managers\Dnf::class;

    protected Stringable $raw;

    protected Collection $selections;

    protected Collection $errors;

    protected $signature = 'dnf:makecache';

    protected $description = 'DNF utility to generate the metadata cache.';

    final public function handle()
    {
        $this->process();
    }

    final protected function process(): void
    {
        $task = 'Generate DNF cache';

        $this->task(
            $task,
            fn () => $this->runShellCommand()
        );
    }

    final protected function runShellCommand(): void
    {
        $command = sprintf('%s makecache %s 2>&1', $this->base(), $this->prepareForwardableOptions());

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            'Running shell command: '.$this->info($command);
        }

        $this->raw = Str::of(shell_exec($command));
    }
}
