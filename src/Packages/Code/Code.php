<?php

namespace OperatingSystems\Packages\Code;

use OperatingSystems\Packages\Package;
use OperatingSystems\Packages\Snap\Snap;

class Code extends Package
{
    protected string $name = 'Code';

    protected ?string $command = 'code';

    protected ?string $preferredManager = Snap::class;
}
