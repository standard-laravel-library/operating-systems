<?php

namespace OperatingSystems\Packages\Code\Candidates;

use OperatingSystems\Packages\Candidates;

class Snap extends Candidates\Snap
{
    protected string $name = 'code';

    protected array $flags = ['--classic' => true];
}
