<?php

namespace OperatingSystems\Packages\Xrandr\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'xrandr';
}
