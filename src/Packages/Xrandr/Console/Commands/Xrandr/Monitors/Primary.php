<?php

namespace OperatingSystems\Packages\Xrandr\Console\Commands\Xrandr\Monitors;

use Illuminate\Support\Str;
use OperatingSystems\Commands\Command;
use Illuminate\Support\Facades\Artisan;
use OperatingSystems\Packages\Xrandr\Console\Commands\Xrandr;

class Primary extends Command
{
    protected $signature = 'xrandr:monitors:primary';

    protected $description = 'Report the primary monitor.';

    public function handle()
    {
        $this->fire();
        $this->displayOutput();
    }

    protected function fire()
    {
        Artisan::call(
            Xrandr::class,
        );

        $this->raw = Str::of(Artisan::output());
    }

    protected function displayOutput()
    {
        $needle = 'connected primary';
        $line = collect($this->raw->explode("\n"))->first(fn ($line) => str_contains($line, $needle));

        $this->line(
            Str::of($line)->before($needle)->trim()
        );
    }
}
