<?php

namespace OperatingSystems\Packages\Xrandr\Console\Commands\Xrandr;

use OperatingSystems\Commands\Command;
use Illuminate\Support\Facades\Artisan;
use OperatingSystems\Packages\Xrandr\Console\Commands\Xrandr;

class Monitors extends Command
{
    protected $signature = 'xrandr:monitors
                            { --active : Report information about currently active monitors. }
    ';

    protected $description = 'Report information about monitors.';

    public function handle()
    {
        $option = $this->option('active') ? 'listactivemonitors' : 'listmonitors';

        Artisan::call(
            Xrandr::class,
            ['--' . $option => true ]
        );

        $this->line(Artisan::output());
    }
}
