<?php

namespace OperatingSystems\Packages\Xrandr\Console\Commands;

use Illuminate\Support\Str;
use OperatingSystems\Commands\Command;

class Xrandr extends Command
{
    protected $signature = 'xrandr
                            { --listmonitors : Report information about all defined monitors. }
                            { --listactivemonitors : Report information about currently active monitors. }
    ';

    protected $description = 'Manage the size, orientation, and/or reflection of the outputs for a screen..';

    protected string $binary = 'xrandr';

    protected array $forwardableOptions = [
        'listmonitors',
        'listactivemonitors',
    ];

    public function handle()
    {
        $options = $this->prepareForwardableOptions();

        $command = sprintf(
            '%s %s 2>&1',
            $this->base(),
            empty($options) ? '' : ' '.$options,
        );

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        $this->raw = Str::of(shell_exec($command));

        $this->line($this->raw);
    }
}
