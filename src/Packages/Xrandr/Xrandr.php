<?php

namespace OperatingSystems\Packages\Xrandr;

use OperatingSystems\Packages\Package;

class Xrandr extends Package
{
    protected string $name = 'Xrandr';

    protected ?string $command = 'xrandr';
}
