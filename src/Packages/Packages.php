<?php

namespace OperatingSystems\Packages;

use Illuminate\Support\Collection;
use OperatingSystems\Packages\Bspwm\Bspwm;
use OperatingSystems\Packages\Contracts\Manager;
use OperatingSystems\Packages\Fish\Fish;
use OperatingSystems\Packages\Picom\Picom;
use OperatingSystems\Packages\Polybar\Polybar;

class Packages
{
    protected $packages = [
        Bspwm::class,
        Fish::class,
        Picom::class,
        Polybar::class,
    ];

    protected Collection $available;

    public function __construct()
    {
        $this->available = collect($this->packages)->map(
            fn ($class) => resolve($class)
        );
    }

    public function for(Manager $manager): Collection
    {
        return $this->filter(
            fn ($package) => $package->candidateFor($manager)
        );
    }

    public function candidatesFor(Manager $manager): Collection
    {
        return $this->for($manager)->map(
            fn ($package) => $package->candidateFor($manager)
        );
    }

    public function __call($method, $parameters): Collection | bool | int | string | null
    {
        return $this->available->{$method}(...$parameters);
    }
}
