<?php

namespace OperatingSystems\Packages\Zsh\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'zsh';
}
