<?php

namespace OperatingSystems\Packages\Zsh\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'zsh';
}
