<?php

namespace OperatingSystems\Packages\Zsh;

use OperatingSystems\Packages\Package;

class Zsh extends Package
{
    protected string $name = 'Zsh';

    protected ?string $command = 'zsh';
}
