<?php

namespace OperatingSystems\Packages\Nfs;

use OperatingSystems\Packages\Package;

class Nfs extends Package
{
    protected string $name = 'NFS';

    protected ?string $command = 'nfs';
}
