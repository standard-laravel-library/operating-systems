<?php

namespace OperatingSystems\Packages\Nfs\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'nfs';
}
