<?php

namespace OperatingSystems\Packages\Nfs\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'nfs-utils';
}
