<?php

namespace OperatingSystems\Packages\AptCache\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\AptCache\AptCache;

class Policy extends Command
{
    protected string $binary = 'apt-cache';

    protected string $package = AptCache::class;

    protected Stringable $raw;

    protected Collection $selections;

    protected Collection $found;

    protected Collection $errors;

    protected $signature = 'apt-cache:policy {packages?*}
                            {--j|json : Format the response as JSON. }
    ';

    protected $description = 'APT installed & installable package information.';

    public function handle()
    {
        $this->init();
        $this->process();
    }

    final protected function init(): void
    {
        $this->selections = collect($this->argument('packages'));
        $this->found = collect();
        $this->errors = collect();
    }

    final protected function process(): void
    {
        $this->search();
    }

    final protected function search(): bool
    {
        $this->runShellCommand();
        $this->displayOutput();

        return $this->errors->isEmpty();
    }

    final protected function recordErrors(): void
    {
        $this->errors = $this->raw->explode(PHP_EOL)->filter(
            fn ($line) => Str::of($line)->startsWith('N:')
        );
    }

    final protected function asTable(): void
    {
        $this->table(
            array_keys($this->found->first()),
            $this->found->toArray(),
        );
    }

    final protected function asJson(): void
    {
        $this->line(
            $this->found->toJson()
        );
    }

    protected function recordResults(): void
    {
        tap($this->raw, function ($raw) {
            $separator = '########START########';
            $this->selections->each(function ($selection) use (&$raw, $separator) {
                $originalStartingLine = $selection.':'.PHP_EOL;
                $newStartingLines = $separator.PHP_EOL.$selection.PHP_EOL;

                $raw = $raw->replace($originalStartingLine, $newStartingLines);
            });

            $raw->explode($separator)->filter()->map(function ($packageBlock) {
                $lines = Str::of($packageBlock)->explode(PHP_EOL)->filter()->values();
                $version = Str::of($lines->get(1))->replace('Installed: ', '');

                $this->found->push([
                    'name' => $lines->get(0),
                    'version' => $version->contains('(none)') ? null : $version,
                ]);
            });
        });
    }

    final protected function displayOutput(): void
    {
        $this->wantsJson() ? $this->asJson() : $this->asTable();

        $this->displayErrors();
    }

    final protected function wantsJson(): bool
    {
        return $this->option('json');
    }

    final protected function wantsTable(): bool
    {
        return ! $this->wantsJson();
    }

    final protected function displayErrors(): void
    {
        if ($this->wantsJson()) {
            return;
        }

        if ($this->errors->isNotEmpty()) {
            $this->newLine();
        }

        $this->errors->each(
            fn ($line) => $this->error(Str::of($line)->replace('N:', ''))
        );
    }

    final protected function runShellCommand(): void
    {
        $command = sprintf(
            '%s policy --quiet=0 %s 2>&1',
            $this->base(),
            $this->selections->join(' '),
        );

        if ($this->getOutput()->isDebug()) {
            'Running shell command: '.$this->info($command);
        }

        $this->raw = Str::of(shell_exec($command));

        $this->recordResults();
        $this->recordErrors();
    }
}
