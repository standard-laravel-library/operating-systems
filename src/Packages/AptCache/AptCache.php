<?php

namespace OperatingSystems\Packages\AptCache;

use OperatingSystems\Packages\Package;

class AptCache extends Package
{
    protected string $name = 'APT Cache';

    protected ?string $command = 'apt-cache';
}
