<?php

namespace OperatingSystems\Packages\Mpv\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'mpv';
}
