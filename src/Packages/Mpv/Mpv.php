<?php

namespace OperatingSystems\Packages\Mpv;

use OperatingSystems\Packages\Package;

class Mpv extends Package
{
    protected string $name = 'MPV';

    protected ?string $command = 'mpv';
}
