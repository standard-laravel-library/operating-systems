<?php

namespace OperatingSystems\Packages\Firefox;

use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Package;

class Firefox extends Package
{
    protected string $name = 'Firefox';

    protected ?string $command = '';

    protected ?string $preferredManager = Flatpak::class;
}
