<?php

namespace OperatingSystems\Packages\Firefox\Candidates;

use OperatingSystems\Packages\Candidates;
use OperatingSystems\Packages\Flatpak\Repositories\Contracts\Flathub;

class Flatpak extends Candidates\Flatpak
{
    protected string $name = 'org.mozilla.firefox';

    protected string $repository = Flathub::class;
}
