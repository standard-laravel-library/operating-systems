<?php

namespace OperatingSystems\Packages\Firefox\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'firefox';
}
