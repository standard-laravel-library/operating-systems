<?php

namespace OperatingSystems\Packages\Alacritty\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'alacritty';
}
