<?php

namespace OperatingSystems\Packages\Alacritty\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'alacritty';

    protected ?string $repository = 'aslatter/ppa';
}
