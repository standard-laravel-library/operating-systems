<?php

namespace OperatingSystems\Packages\Alacritty\Console\Commands;

use Illuminate\Support\Str;
use OperatingSystems\Commands\Command;

class Alacritty extends Command
{
    protected $signature = 'alacritty';

    protected $description = 'A fast, cross-platform, OpenGL terminal emulator.';

    protected string $binary = 'alacritty';

    public function handle()
    {
        $command = sprintf(
            'alacritty 2>&1',
            $this->base(),
        );

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        $this->raw = Str::of(shell_exec($command));
    }
}
