<?php

namespace OperatingSystems\Packages\Alacritty;

use OperatingSystems\Packages\Package;

class Alacritty extends Package
{
    protected string $name = 'Alacritty';

    protected ?string $command = 'alacritty';
}
