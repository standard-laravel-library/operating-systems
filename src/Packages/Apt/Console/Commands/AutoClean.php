<?php

namespace OperatingSystems\Packages\Apt\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Managers;

class AutoClean extends Command
{
    protected string $binary = 'apt-get';

    protected bool $root = true;

    protected string $package = Managers\Apt::class;

    protected Stringable $raw;

    protected Collection $selections;

    protected Collection $errors;

    protected $signature = 'apt:autoclean
                            { --y|yes : Automatic yes to prompts }
    ';

    protected $description = 'APT package auto clean utility.';

    protected array $forwardableOptions = [
        'yes',
    ];

    final public function handle()
    {
        $this->process();
    }

    final protected function process(): void
    {
        $task = 'Clean cache';

        $this->task(
            $task,
            fn () => $this->runShellCommand()
        );
    }

    final protected function runShellCommand(): void
    {
        $command = sprintf('%s autoclean %s 2>&1', $this->base(), $this->prepareForwardableOptions());

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            'Running shell command: '.$this->info($command);
        }

        $this->raw = Str::of(shell_exec($command));
    }
}
