<?php

namespace OperatingSystems\Packages\Apt\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Managers;

class Update extends Command
{
    protected string $binary = 'apt-get';

    protected bool $root = true;

    protected string $package = Managers\Apt::class;

    protected Stringable $raw;

    protected Collection $selections;

    protected Collection $errors;

    protected $signature = 'apt:update';

    protected $description = 'APT package update utility.';

    final public function handle()
    {
        $this->process();
    }

    final protected function process(): void
    {
        $task = 'Update cache';

        $this->task(
            $task,
            fn () => $this->runShellCommand()
        );
    }

    final protected function runShellCommand(): void
    {
        $command = sprintf('%s update 2>&1', $this->base());

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            'Running shell command: '.$this->info($command);
        }

        $this->raw = Str::of(shell_exec($command));
    }
}
