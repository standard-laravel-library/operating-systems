<?php

namespace OperatingSystems\Packages\Apt\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Contracts\Manager;
use OperatingSystems\Packages\Managers;
use OperatingSystems\Packages\Packages;

class AutoRemove extends Command
{
    protected string $binary = 'apt-get';

    protected bool $root = true;

    protected string $package = Managers\Apt::class;

    protected Stringable $raw;

    protected Collection $selections;

    protected Collection $errors;

    protected bool $confirmed;

    protected $signature = 'apt:autoremove {packages?*}
                            { --purge : Clean up configuration files }
                            { --y|yes : Automatic yes to prompts }
    ';

    protected $description = 'APT package auto remove utility.';

    protected array $forwardableOptions = [
        'yes',
        'purge',
    ];

    final public function handle()
    {
        $this->init();
        $this->prompt();
        $this->process();
    }

    final protected function init(): void
    {
        $this->selections = collect($this->argument('packages'));
        $this->errors = collect();
        $this->confirmed = $this->option('no-interaction') || $this->selections->isNotEmpty();
    }

    final protected function prompt(): void
    {
        $this->selections->isNotEmpty() || $this->isConfirmed() || $this->displayMenu();
    }

    final protected function displayMenu(): void
    {
        $menu = $this->menu('Packages');

        $this->addPackageCheckbox($menu);

        $this->confirmed = (bool) $menu->addOption('remove', 'Remove')->open();

        if ($this->isConfirmed()) {
            ($this->selections->isEmpty() && (bool) $this->menu('Would you like to remove all stale packages?')->addOption('yes', 'Yes')->open())
            || $this->displayMenu();
        }
    }

    final protected function process(): void
    {
        if ($this->isCancelled()) {
            return;
        }

        $task = $this->selections->isEmpty() ? 'Remove packages' : 'Remove '.$this->selections->join(', ', ', & ');

        $this->task(
            $task,
            fn () => $this->removeSelections()
        );
    }

    private function isConfirmed(): bool
    {
        return $this->confirmed;
    }

    private function isCancelled(): bool
    {
        return ! $this->isConfirmed();
    }

    final protected function removeSelections(): bool
    {
        $this->runShellCommand();
        $this->displayOutput();

        return $this->errors->isEmpty();
    }

    final protected function recordErrors(): void
    {
        $this->errors = $this->raw->explode(PHP_EOL)->filter(
            fn ($line) => Str::of($line)->startsWith('E:')
        );
    }

    final protected function displayOutput(): void
    {
        $this->displayErrors();
    }

    final protected function displayErrors(): void
    {
        if ($this->errors->isNotEmpty()) {
            $this->newLine();
        }

        $this->errors->each(
            fn ($line) => $this->error(Str::of($line)->replace('E:', ''))
        );
    }

    final protected function manager(): Manager
    {
        return resolve($this->package);
    }

    final protected function available()
    {
        return resolve(Packages::class)->candidatesFor($this->manager());
    }

    final protected function addPackageCheckbox($menu): void
    {
        $handler = fn ($menu) => $this->toggleSelection($menu->getSelectedItem()->getText());

        $this->available()->each(
            fn ($candidate) => $menu->addCheckboxItem($candidate->name(), $handler)
        );
    }

    final protected function toggleSelection(string $selection): void
    {
        $this->selections = $this->selections->contains($selection)
            ? $this->selections->reject(fn ($package) => $package === $selection)
            : $this->selections->push($selection);
    }

    final protected function runShellCommand(): void
    {
        $command = sprintf('%s autoremove %s %s 2>&1', $this->base(), $this->prepareForwardableOptions(), $this->selections->join(' '));

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            'Running shell command: '.$this->info($command);
        }

        $this->raw = Str::of(shell_exec($command));
        $this->recordErrors();
    }
}
