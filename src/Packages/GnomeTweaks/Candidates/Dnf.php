<?php

namespace OperatingSystems\Packages\GnomeTweaks\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'gnome-tweaks';
}
