<?php

namespace OperatingSystems\Packages\GnomeTweaks\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'gnome-tweaks';
}
