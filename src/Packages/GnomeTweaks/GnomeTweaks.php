<?php

namespace OperatingSystems\Packages\GnomeTweaks;

use OperatingSystems\Packages\Package;

class GnomeTweaks extends Package
{
    protected string $name = 'Gnome Tweaks';

    protected ?string $command = '';
}
