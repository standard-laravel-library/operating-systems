<?php

namespace OperatingSystems\Packages\Telegram\Candidates;

use OperatingSystems\Packages\Candidates;
use OperatingSystems\Packages\Flatpak\Repositories\Contracts\Flathub;

class Flatpak extends Candidates\Flatpak
{
    protected string $name = 'org.telegram.desktop';

    protected string $repository = Flathub::class;
}
