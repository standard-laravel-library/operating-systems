<?php

namespace OperatingSystems\Packages\Telegram;

use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Package;

class Telegram extends Package
{
    protected string $name = 'Telegram';

    protected ?string $command = '';

    protected ?string $preferredManager = Flatpak::class;
}
