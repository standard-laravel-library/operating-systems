<?php

namespace OperatingSystems\Packages\Contracts;

interface Manager
{
    public function install(Package $package): bool;

    public function update(Package $package): bool;

    public function uninstall(Package $package): bool;

    public function defaultRepository(): Repository;
}
