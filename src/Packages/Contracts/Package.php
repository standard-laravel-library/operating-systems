<?php

namespace OperatingSystems\Packages\Contracts;

use OperatingSystems\Commands\Contracts\Command;
use OperatingSystems\Packages\Candidates\Contracts\Candidate;

interface Package
{
    public function name(): string;

    public function command(): ?Command;

    public function preferredManager(): Manager;

    public function repositoryFor(Manager $manager): ?string;

    public function installFlagsFor(Manager $manager): ?string;

    public function candidateFor(Manager $manager): Candidate;
}
