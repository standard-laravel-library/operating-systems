<?php

namespace OperatingSystems\Packages\Dunst;

use OperatingSystems\Packages\Package;

class Dunst extends Package
{
    protected string $name = 'Dunst';

    protected ?string $command = 'dunst';
}
