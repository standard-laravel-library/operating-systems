<?php

namespace OperatingSystems\Packages\Dunst\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'dunst';
}
