<?php

namespace OperatingSystems\Packages\Dunst\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'dunst';
}
