<?php

namespace OperatingSystems\Packages\Picom;

use OperatingSystems\Packages\Package;

class Picom extends Package
{
    protected string $name = 'PICOM';

    protected ?string $command = 'picom';
}
