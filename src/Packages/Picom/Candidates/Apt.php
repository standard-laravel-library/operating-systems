<?php

namespace OperatingSystems\Packages\Picom\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'picom';
}
