<?php

namespace OperatingSystems\Packages\Picom\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'picom';
}
