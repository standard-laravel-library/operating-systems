<?php

namespace OperatingSystems\Packages\Git\Console\Commands;

use Illuminate\Support\Collection;
use OperatingSystems\Commands\Command;

class Pull extends Command
{
    protected string $binary = 'git';

    // TODO: We should be able to specify the branch, so directory maybe becomes an option and branch is the single argument?
    protected $signature = 'git:pull {directory?}
                            { --autostash : Automatically create a temporary stash entry before the operation begins and apply it after the operation ends. }
                            { --r|rebase : Rebase the current branch on top of the upstream branch after fetching. }
    ';

    protected $description = 'Fetch from and integrate another repository or a local branch.';

    protected array $forwardableOptions = [
        'autostash',
        'rebase',
    ];

    protected Collection $errors;

    public function handle()
    {
        $this->init();
        $this->process();
    }

    final protected function init(): void
    {
        $this->errors = collect();
    }

    private function process()
    {
        $task = 'Pulling';

        $this->task(
            $task,
            fn () => $this->pull()
        );
    }

    private function pull()
    {
        $this->runShellCommand();
    }

    final protected function runShellCommand(): void
    {
        $options = $this->prepareForwardableOptions();

        // TODO: When this is moved to Process, we should be able to provide the cwd instead of using -C

        $command = sprintf(
            '%s%s pull%s 2>&1',
            $this->base(),
            $this->argument('directory') ? ' -C '.$this->argument('directory') : '',
            empty($options) ? '' : ' '.$options,
        );

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        shell_exec($command);
    }
}
