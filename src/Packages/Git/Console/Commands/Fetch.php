<?php

namespace OperatingSystems\Packages\Git\Console\Commands;

use Illuminate\Support\Collection;
use OperatingSystems\Commands\Command;

class Fetch extends Command
{
    protected string $binary = 'git';

    protected $signature = 'git:fetch {directory?}
                            { --a|all : Fetch all remotes. }
                            { --p|prune : Remove any remote-tracking references that no longer exist on the remote. }
                            { --P|prune-tags : Remove anylocal tags that no longer exist on the remote if --prunce is enabled. }
                            { --t|tags : Fetch all tags from the remote. }
    ';

    protected $description = 'Download objects and refs from another repository.';

    protected array $forwardableOptions = [
        'all',
        'prune',
        'prune-tags',
        'tags',
    ];

    protected Collection $errors;

    public function handle()
    {
        $this->init();
        $this->process();
    }

    final protected function init(): void
    {
        $this->errors = collect();
    }

    private function process()
    {
        $task = 'Fetching';

        $this->task(
            $task,
            fn () => $this->fetch()
        );
    }

    private function fetch()
    {
        $this->runShellCommand();
    }

    final protected function runShellCommand(): void
    {
        $options = $this->prepareForwardableOptions();

        $command = sprintf(
            '%s%s fetch%s 2>&1',
            $this->base(),
            $this->argument('directory') ? ' -C '.$this->argument('directory') : '',
            empty($options) ? '' : ' '.$options,
        );

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        shell_exec($command);
    }
}
