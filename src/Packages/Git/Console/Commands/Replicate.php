<?php

namespace OperatingSystems\Packages\Git\Console\Commands;

use Illuminate\Support\Collection;
use OperatingSystems\Commands\Command;

class Replicate extends Command
{
    protected string $binary = 'git';

    protected $signature = 'git:clone {url} {directory?}
                            { --depth= : Create a shallow clone with a history truncated to the specified number of commits. }
    ';

    protected $description = 'Clone a git repository.';

    protected array $forwardableOptions = [
        'depth',
    ];

    protected Collection $errors;

    public function handle()
    {
        $this->init();
        $this->process();
    }

    final protected function init(): void
    {
        $this->errors = collect();
    }

    private function process()
    {
        $task = 'Clone '.$this->argument('url');

        $this->task(
            $task,
            fn () => $this->clone()
        );
    }

    private function clone()
    {
        $this->runShellCommand();
    }

    final protected function runShellCommand(): void
    {
        $options = $this->prepareForwardableOptions();

        $command = sprintf(
            '%s clone%s %s%s 2>&1',
            $this->base(),
            empty($options) ? '' : ' '.$options,
            $this->argument('url') ? $this->argument('url') : '',
            $this->argument('directory') ? ' '.$this->argument('directory') : '',
        );

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        shell_exec($command);
    }
}
