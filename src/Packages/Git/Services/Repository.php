<?php

namespace OperatingSystems\Packages\Git\Services;

use Illuminate\Support\Facades\Artisan;
use OperatingSystems\Commands\Git as Commands;

class Repository
{
    protected string $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function clone(string $url, ?int $depth = null)
    {
        Artisan::call(
            Commands\Replicate::class,
            collect(['url' => $url, 'directory' => $this->path, '--depth' => $depth])->filter()->toArray()
        );

        return $this;
    }

    public function fetch(bool $all = false, bool $prune = false, bool $pruneTags = false, bool $tags = false)
    {
        Artisan::call(
            Commands\Fetch::class,
            ['directory' => $this->path, '--all' => $all, '--prune' => $prune, '--prune-tags' => $pruneTags, '--tags' => $tags]
        );

        return $this;
    }

    public function pull(bool $rebase = false, bool $autostash = false)
    {
        Artisan::call(
            Commands\Pull::class,
            ['directory' => $this->path, '--rebase' => $rebase, '--autostash' => $autostash]
        );

        return $this;
    }
}
