<?php

namespace OperatingSystems\Packages\Git\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'git';
}
