<?php

namespace OperatingSystems\Packages\Git;

use Illuminate\Support\Facades\Artisan;
use OperatingSystems\Commands\Git\Install;
use OperatingSystems\Packages\Contracts\Package;
use OperatingSystems\Packages\Git\Services\Repository;
use OperatingSystems\Packages\Manager;

class Git extends Manager
{
    protected bool $root = true;

    // TODO: Is this property needed anymore for packages?
    // TODO: Should there be some sort of array for the commands?
    // TODO: Should commands live in each Package directory instead?
    // TODO: This could then allow auto-discovery of commands which might be useful?
    protected ?string $command = 'git';

    protected string $installer = Install::class;

    public function install(Package $package): bool
    {
        // This concept may not be ideal in the end, might not make sense for these classes
        // to have these actions as they require sudo privileges. However, we might also
        // we may also be able to add instructions to add current user to the sudo
        // user list which then would allow these to happen in the application
        $installer = $this->installer();
        $candidate = $package->candidateFor($installer->manager());

        Artisan::call(
            $installer::class,
            ['packages' => $candidate->name(), '-y' => true]
        );

        return true;
    }

    public function open($path)
    {
        return new Repository($path);
    }
}
