<?php

namespace OperatingSystems\Packages\Git\Facades;

use Illuminate\Support\Facades\Facade;
use OperatingSystems\Packages;

class Git extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Packages\Git\Git::class;
    }
}
