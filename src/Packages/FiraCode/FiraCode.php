<?php

namespace OperatingSystems\Packages\FiraCode;

use OperatingSystems\Packages\Package;

class FiraCode extends Package
{
    protected string $name = 'FiraCode';

    protected ?string $command = '';
}
