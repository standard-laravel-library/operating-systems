<?php

namespace OperatingSystems\Packages\FiraCode\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'fira-code-fonts';
}
