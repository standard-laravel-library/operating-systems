<?php

namespace OperatingSystems\Packages\FiraCode\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'fonts-firacode';
}
