<?php

namespace OperatingSystems\Packages\LibNotify;

use OperatingSystems\Packages\Package;

class LibNotify extends Package
{
    protected string $name = 'libnotify';

    protected ?string $command = 'notify-send';
}
