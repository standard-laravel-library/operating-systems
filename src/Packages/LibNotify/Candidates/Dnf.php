<?php

namespace OperatingSystems\Packages\LibNotify\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'libnotify';
}
