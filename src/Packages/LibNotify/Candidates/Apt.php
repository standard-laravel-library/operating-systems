<?php

namespace OperatingSystems\Packages\LibNotify\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'libnotify';
}
