<?php

namespace OperatingSystems\Packages\LibNotify\Console\Commands;

use OperatingSystems\Commands\Command;

class NotifySend extends Command
{
    protected string $binary = 'notify-send';

    protected bool $root = false;

    protected $signature = 'notify-send {message}
                            { --c|category : Specifies the notification category. }
                            { --i|icon : Specifies an icon or stock icon to display. }
                            { --t|expire-time : Specifies the timeout in milliseconds. }
                            { --u|urgency= : Specifies the urgency level (low, normal, critical). }
    ';

    protected $description = 'Send desktop notifications to the user via a notification daemon.';

    protected array $forwardableOptions = [
        'icon',
        'urgency',
        'category',
        'expire-time',
    ];

    final public function handle()
    {
        $this->runShellCommand();
    }

    final protected function runShellCommand(): void
    {
        $options = $this->prepareForwardableOptions();

        $command = sprintf(
            '%s %s %s 2>&1',
            $this->base(),
            $this->argument('message'),
            empty($options) ? '' : ' '.$options,
        );

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        shell_exec($command);
    }
}
