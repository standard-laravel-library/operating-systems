<?php

namespace OperatingSystems\Packages;

use Illuminate\Support\Collection;
use OperatingSystems\Commands\Contracts\Command;
use OperatingSystems\Facades\OperatingSystem;
use OperatingSystems\Packages\Candidates\Contracts\Candidate;
use OperatingSystems\Packages\Contracts\Manager;
use OperatingSystems\Packages\Contracts\Package as PackageContract;

abstract class Package implements PackageContract
{
    protected string $name;

    protected array $repositories = [];

    protected ?string $command;

    // TODO: We'll want a simple way for a user to define this per package (i.e. I currently want Code installed as a Snap)
    protected ?string $preferredManager = null;

    protected array $installFlags = [];

    final public function name(): string
    {
        return $this->name;
    }

    final public function command(): ?Command
    {
        return $this->command ? resolve($this->command) : null;
    }

    final public function repositoryFor(Manager $manager): ?string
    {
        return array_key_exists($manager::class, $this->repositories) ? $this->repositories[$manager::class] : null;
    }

    final public function installFlagsFor(Manager $manager): ?string
    {
        return array_key_exists($manager::class, $this->installFlags) ? $this->installFlags[$manager::class] : null;
    }

    final public function candidates(): Collection
    {
        $namespace = (new \ReflectionClass($this::class))->getNamespaceName();

        return collect(['Apt', 'Flatpak', 'Snap', 'Dnf'])->filter(
            fn ($type) => class_exists($namespace.'\\Candidates\\'.$type)
        )->map(
            fn ($type) => resolve($namespace.'\\Candidates\\'.$type)
        );
    }

    final public function candidateFor(Manager $manager): Candidate
    {
        return $this->candidates()->first(
            fn ($candidate) => is_a($candidate->manager(), $manager::class)
        );
    }

    final public function preferredManager(): Manager
    {
        $preferredManager = $this->preferredManager ? resolve($this->preferredManager) : OperatingSystem::defaultPackageManager();

        return $this->candidates()->map->manager()->first(
            fn (Manager $manager) => is_a($manager, $preferredManager::class),
        );
    }
}
