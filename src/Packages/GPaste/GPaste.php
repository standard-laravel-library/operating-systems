<?php

namespace OperatingSystems\Packages\GPaste;

use OperatingSystems\Packages\Package;

class GPaste extends Package
{
    protected string $name = 'GPaste';

    protected ?string $command = '';
}
