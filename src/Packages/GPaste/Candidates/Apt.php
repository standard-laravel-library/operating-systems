<?php

namespace OperatingSystems\Packages\GPaste\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'gpaste';
}
