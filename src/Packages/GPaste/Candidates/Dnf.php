<?php

namespace OperatingSystems\Packages\GPaste\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'gpaste';
}
