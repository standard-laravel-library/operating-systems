<?php

namespace OperatingSystems\Packages\Neofetch\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'neofetch';
}
