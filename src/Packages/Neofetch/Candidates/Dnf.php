<?php

namespace OperatingSystems\Packages\Neofetch\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'neofetch';
}
