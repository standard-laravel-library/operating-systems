<?php

namespace OperatingSystems\Packages\Neofetch;

use OperatingSystems\Packages\Package;

class Neofetch extends Package
{
    protected string $name = 'Neofetch';

    protected ?string $command = 'neofetch';
}
