<?php

namespace OperatingSystems\Packages\Screenshot\Candidates;

use OperatingSystems\Packages\Candidates;

class Flatpak extends Candidates\Flatpak
{
    protected string $name = 'org.gnome.Screenshot';

    protected string $repository = Flathub::class;
}
