<?php

namespace OperatingSystems\Packages\Screenshot;

use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Package;

class Screenshot extends Package
{
    protected string $name = 'Screenshot';

    protected ?string $command = '';

    protected ?string $preferredManager = Flatpak::class;
}
