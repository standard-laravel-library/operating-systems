<?php

namespace OperatingSystems\Packages\MateScreenshot;

use OperatingSystems\Packages\Package;

class MateScreenshot extends Package
{
    protected string $name = 'Mate Screenshot';

    protected ?string $command = '';
}
