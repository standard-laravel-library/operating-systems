<?php

namespace OperatingSystems\Packages\MateScreenshot\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'mate-media';
}
