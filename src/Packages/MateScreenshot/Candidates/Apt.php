<?php

namespace OperatingSystems\Packages\MateScreenshot\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'mate-media';
}
