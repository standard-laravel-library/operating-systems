<?php

namespace OperatingSystems\Packages\PapirusIconTheme;

use OperatingSystems\Packages\Package;

class PapirusIconTheme extends Package
{
    protected string $name = 'Papirus Icon Theme';
}
