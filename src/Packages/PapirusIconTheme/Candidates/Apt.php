<?php

namespace OperatingSystems\Packages\PapirusIconTheme\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'papirus-icon-theme';
}
