<?php

namespace OperatingSystems\Packages\PapirusIconTheme\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'papirus-icon-theme';
}
