<?php

namespace OperatingSystems\Packages\Locators\Contracts;

interface Locator
{
    public function find(string $command): string;
}
