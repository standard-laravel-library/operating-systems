<?php

namespace OperatingSystems\Packages\Locators\Support;

use OperatingSystems\Kernels\Contracts\Kernel;
use OperatingSystems\Kernels\Linux;
use OperatingSystems\Packages\Locators\Contracts\Locator;
use OperatingSystems\Packages\Locators\Which;

class Finder
{
    public static function detect(Kernel $kernel): Locator
    {
        return match ($kernel::class) {
            Linux::class => static::linux(),
        };
    }

    private static function linux(): Locator
    {
        return new Which;
    }
}
