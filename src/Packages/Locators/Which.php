<?php

namespace OperatingSystems\Packages\Locators;

use OperatingSystems\Packages\Locators\Contracts\Locator;
use OperatingSystems\Packages\Package;

class Which extends Package implements Locator
{
    protected ?string $command = 'which';

    public function find(string $command): string
    {
        $result = shell_exec(
            sprintf('which %s', $command)
        );

        return str_replace(PHP_EOL, '', $result);
    }
}
