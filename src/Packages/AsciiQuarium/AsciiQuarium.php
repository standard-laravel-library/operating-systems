<?php

namespace OperatingSystems\Packages\AsciiQuarium;

use OperatingSystems\Packages\Package;

class AsciiQuarium extends Package
{
    protected string $name = 'AsciiQuarium';

    protected ?string $command = 'asciiquarium';
}
