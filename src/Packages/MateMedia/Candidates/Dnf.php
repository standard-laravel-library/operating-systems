<?php

namespace OperatingSystems\Packages\MateMedia\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'mate-media';
}
