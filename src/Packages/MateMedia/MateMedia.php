<?php

namespace OperatingSystems\Packages\MateMedia;

use OperatingSystems\Packages\Package;

class MateMedia extends Package
{
    protected string $name = 'Mate Media';

    protected ?string $command = '';
}
