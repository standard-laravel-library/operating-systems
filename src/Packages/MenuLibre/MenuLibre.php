<?php

namespace OperatingSystems\Packages\MenuLibre;

use OperatingSystems\Packages\Package;

class MenuLibre extends Package
{
    protected string $name = 'Menu Libre';

    protected ?string $command = 'menulibre';
}
