<?php

namespace OperatingSystems\Packages\MenuLibre\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'menulibre';
}
