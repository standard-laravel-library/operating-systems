<?php

namespace OperatingSystems\Packages\Flatseal\Candidates;

use OperatingSystems\Packages\Candidates;
use OperatingSystems\Packages\Flatpak\Repositories\Contracts\Flathub;

class Flatpak extends Candidates\Flatpak
{
    protected string $name = 'com.github.tchx84.Flatseal';

    protected string $repository = Flathub::class;
}
