<?php

namespace OperatingSystems\Packages\Flatseal;

use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Package;

class Flatseal extends Package
{
    protected string $name = 'Flatseal';

    protected ?string $command = '';

    protected ?string $preferredManager = Flatpak::class;
}
