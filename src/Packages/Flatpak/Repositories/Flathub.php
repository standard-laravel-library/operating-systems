<?php

namespace OperatingSystems\Packages\Flatpak\Repositories;

class Flathub extends Repository
{
    protected string $url = 'https://flathub.org/repo/flathub.flatpakrepo';
}
