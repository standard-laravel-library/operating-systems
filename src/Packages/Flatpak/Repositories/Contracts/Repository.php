<?php

namespace OperatingSystems\Packages\Flatpak\Repositories\Contracts;

use OperatingSystems\Packages\Contracts;

interface Repository extends Contracts\Repository
{
    public function name(): string;

    public function url(): string;
}
