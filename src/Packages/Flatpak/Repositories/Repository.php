<?php

namespace OperatingSystems\Packages\Flatpak\Repositories;

use Illuminate\Support\Str;

abstract class Repository implements Contracts\Repository
{
    protected string $url;

    public function name(): string
    {
        return Str::of(class_basename($this))->lower();
    }

    public function url(): string
    {
        return $this->url;
    }
}
