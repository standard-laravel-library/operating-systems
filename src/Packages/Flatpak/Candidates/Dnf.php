<?php

namespace OperatingSystems\Packages\Flatpak\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'flatpak';
}
