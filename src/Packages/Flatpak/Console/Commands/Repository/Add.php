<?php

namespace OperatingSystems\Packages\Flatpak\Console\Commands\Repository;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Flatpak\Flatpak;

class Add extends Command
{
    protected string $binary = 'flatpak';

    protected bool $root = true;

    protected string $package = Flatpak::class;

    protected Stringable $raw;

    protected Collection $selections;

    protected Collection $errors;

    protected $signature = 'flatpak:repository:add {name} {location}
                            { --disable : Disable the remote. }
                            { --user : Work on the user installation. }
                            { --system : Work on the system-wide installation (default). }
                            { --if-not-exists : Do nothing if the provided remote exists. }
    ';

    protected $description = 'Flatpak utility to add package repositories.';

    protected array $forwardableOptions = [
        'disable',
        'user',
        'system',
        'if-not-exists',
    ];

    final public function handle()
    {
        $this->init();
        $this->process();
    }

    final protected function init(): void
    {
        $this->errors = collect();
    }

    final protected function process(): void
    {
        $task = sprintf('Add %s repository', $this->argument('name'));

        $this->task(
            $task,
            fn () => $this->addRepository()
        );
    }

    final protected function addRepository(): bool
    {
        $this->runShellCommand();
        $this->displayOutput();

        return $this->errors->isEmpty();
    }

    final protected function recordErrors(): void
    {
        $this->errors = $this->raw->explode(PHP_EOL)->filter(
            fn ($line) => Str::of($line)->startsWith('E:')
        );
    }

    final protected function displayOutput(): void
    {
        $this->displayErrors();
    }

    final protected function displayErrors(): void
    {
        if ($this->errors->isNotEmpty()) {
            $this->newLine();
        }

        $this->errors->each(
            fn ($line) => $this->error(Str::of($line)->replace('E:', ''))
        );
    }

    final protected function runShellCommand(): void
    {
        $options = $this->prepareForwardableOptions();

        $command = sprintf(
            '%s remote-add %s %s %s 2>&1',
            $this->base(),
            empty($options) ? '' : ' '.$options,
            $this->argument('name'),
            $this->argument('location')
        );

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        $this->raw = Str::of(shell_exec($command));
        $this->recordErrors();
    }
}
