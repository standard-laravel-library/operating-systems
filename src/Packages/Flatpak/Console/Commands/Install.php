<?php

namespace OperatingSystems\Packages\Flatpak\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Contracts\Manager;
use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Packages;
use Symfony\Component\Process\Process;

class Install extends Command
{
    protected string $binary = 'flatpak';

    protected bool $root = true;

    protected string $package = Flatpak::class;

    protected Stringable $raw;

    protected Process $process;

    protected string $selections;

    protected Stringable $error;

    protected $signature = 'flatpak:install {package?}
                            { --user : Work on the user installation. }
                            { --system : work on the sytem-wide installation (default). }
                            { --y|assumeyes : Install from the beta channel. }
                            { --reinstall : Install from the candidate channel. }
    ';

    protected $description = 'Flatpak utility to install packages.';

    protected array $forwardableOptions = [
        'user',
        'system',
        'assumeyes',
        'reinstall',
    ];

    final public function handle()
    {
        $this->init();
        $this->process();

        return false;
    }

    public function locate(): string
    {
        // Base implementation doesn't work so overwriting for now
        return 'flatpak';
    }

    final protected function init(): void
    {
        $this->selection = $this->argument('package');
    }

    final protected function process(): bool
    {
        $task = 'Installing '.$this->selection;

        return $this->task(
            $task,
            fn () => $this->installSelection()
        );
    }

    final protected function installSelection(): bool
    {
        $this->runShellCommand();
        $this->displayOutput();

        return $this->successful();
    }

    protected function errored()
    {
        return $this->error->length() && ! $this->error->contains('Skipping');
    }

    protected function successful()
    {
        return ! $this->errored();
    }

    final protected function recordError(): void
    {
        $this->error = Str::of($this->process->getErrorOutput());
    }

    final protected function displayOutput(): void
    {
        $this->displayErrors();
    }

    final protected function displayErrors(): void
    {
        if ($this->successful()) {
            return;
        }

        $this->newLine();
        $this->error($this->error->replace("\n", ''));
    }

    final public function manager(): Manager
    {
        return resolve($this->package);
    }

    final protected function available()
    {
        return resolve(Packages::class)->candidatesFor($this->manager());
    }

    final protected function runShellCommand(): void
    {
        $options = $this->prepareForwardableOptions();

        $this->process = new Process(collect([
            'sudo',
            $this->locate(),
            'install',
            $options,
            $this->selection,
        ])->filter()->toArray());

        $this->process->setTty(false);

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$this->process->getCommandLine());
        }

        $this->process->run();
        // $this->line($result);

        $this->raw = Str::of($this->process->getOutput());
        $this->recordError();
    }
}
