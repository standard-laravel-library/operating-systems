<?php

namespace OperatingSystems\Packages\Flatpak;

use Illuminate\Support\Facades\Artisan;
use OperatingSystems\Packages\Contracts\Package;
use OperatingSystems\Packages\Flatpak\Console\Commands\Install;
use OperatingSystems\Packages\Manager;

class Flatpak extends Manager
{
    protected string $name = 'Flatpak';

    protected ?string $command = 'flatpak';

    protected ?string $defaultRepository = Repositories\Flathub::class;

    protected string $installer = Install::class;

    public function install(Package $package): bool
    {
        // This concept may not be ideal in the end, might not make sense for these classes
        // to have these actions as they require sudo privileges. However, we might also
        // we may also be able to add instructions to add current user to the sudo
        // user list which then would allow these to happen in the application
        $installer = $this->installer();
        $candidate = $package->candidateFor($installer->manager());

        Artisan::call(
            $installer::class,
            [
                'packages' => $candidate->name(),
                '--assumeyes' => true,
            ]
        );

        return true;
    }
}
