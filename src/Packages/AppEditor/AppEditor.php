<?php

namespace OperatingSystems\Packages\AppEditor;

use OperatingSystems\Packages\Package;

class AppEditor extends Package
{
    protected string $name = 'App Editor';

    protected ?string $command = 'appeditor';
}
