<?php

namespace OperatingSystems\Packages\AppEditor\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'appeditor';
}
