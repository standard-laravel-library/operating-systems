<?php

namespace OperatingSystems\Packages\NotoColorEmoji;

use OperatingSystems\Packages\Package;

class NotoColorEmoji extends Package
{
    protected string $name = 'Noto Color Emoji';

    protected ?string $command = '';
}
