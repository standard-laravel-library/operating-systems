<?php

namespace OperatingSystems\Packages\NotoColorEmoji\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'google-noto-emoji-fonts';
}
