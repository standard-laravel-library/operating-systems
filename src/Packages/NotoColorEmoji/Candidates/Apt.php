<?php

namespace OperatingSystems\Packages\NotoColorEmoji\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'fonts-noto-color-emoji';
}
