<?php

namespace OperatingSystems\Packages\LinuxStandardBase\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use OperatingSystems\Commands\LinuxStandardBase as Command;

class LinuxStandardBase
{
    protected string $distribution;

    protected string $description;

    protected string $release;

    protected string $codename;

    public function __construct()
    {
        // Register a binding into the container of an implementation of a PackageManager contract
        // This will check for a config value of a preferred PackageManager to use
        // Otherwise it wall fallback to the OperatingSystem default PackageManager
        // $this->$packageManager = $packageManager;

        $raw = $this->raw();

        $this->distribution = data_get($raw, 'Distributor ID');
        $this->description = data_get($raw, 'Description');
        $this->release = data_get($raw, 'Release');
        $this->codename = data_get($raw, 'Codename');
    }

    protected function raw(): Collection
    {
        Artisan::call(Command::class, ['-a' => true, '-j' => true]);

        return collect(
            json_decode(
                Artisan::output(), true
            )
        );
    }

    final public function toArray(): array
    {
        return [
            'distribution' => $this->distribution,
            'description' => $this->description,
            'release' => $this->release,
            'codename' => $this->codename,
        ];
    }

    final public function __get($property)
    {
        return match ($property) {
            'distribution', 'description', 'release', 'codename' => $this->toArray()[$property],
            default => null
        };
    }
}
