<?php

namespace OperatingSystems\Packages\LinuxStandardBase\Console\Commands;

use Illuminate\Support\Collection;
use OperatingSystems\Commands\Command;
use OperatingSystems\Packages;

class LinuxStandardBase extends Command
{
    protected string $binary = 'lsb_release';

    protected string $package = Packages\LinuxStandardBase::class;

    protected $signature = 'lsb
                            {--a|all : Show all of available information. }
                            {--c|codename : Show the code name of this distribution. }
                            {--d|description : Show the description of this distribution. }
                            {--i|id : Show Distributor ID. }
                            {--r|release : Show release number of this distribution. }
                            {--j|json : Format the response as JSON. }
    ';

    protected $description = 'Print distribution information.';

    protected $columnKeyMap = [
        'Distributor ID' => 'id',
        'Description' => 'description',
        'Codename' => 'codename',
        'Release' => 'release',
    ];

    public function handle()
    {
        $this->option('json') ? $this->asJson() : $this->asTable();
    }

    protected function asTable(): void
    {
        $result = $this->filter();

        $this->table(
            $result->keys()->toArray(),
            [$result->values()->toArray()]
        );
    }

    protected function asJson(): void
    {
        $this->line(
            $this->filter()->toJson()
        );
    }

    protected function raw(): string
    {
        return shell_exec(
            sprintf('%s -a', $this->base())
        );
    }

    protected function result(): Collection
    {
        return collect(explode(
            PHP_EOL,
            $this->raw()
        ))->filter()->mapWithKeys(function ($line) {
            [$key, $value] = explode("\t", $line);

            return [\Illuminate\Support\Str::replace(':', '', $key) => $value];
        });
    }

    public function filter()
    {
        $options = collect($this->columnKeyMap)->filter(
            fn ($value) => $this->option($value) || $this->option('all'),
        );

        return $this->result()->filter(
            fn ($value, $key) => $options->has($key),
        );
    }
}
