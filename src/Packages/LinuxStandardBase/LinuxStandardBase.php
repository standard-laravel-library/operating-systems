<?php

namespace OperatingSystems\Packages\LinuxStandardBase;

use OperatingSystems\Packages\Package;

class LinuxStandardBase extends Package
{
    protected string $name = 'Linux Standard Base';
}
