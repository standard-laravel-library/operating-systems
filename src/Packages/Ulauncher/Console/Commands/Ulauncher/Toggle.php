<?php

namespace OperatingSystems\Packages\Ulauncher\Console\Commands\Ulauncher;

use Illuminate\Support\Str;
use OperatingSystems\Commands\Command;
use Symfony\Component\Process\Process;

class Toggle extends Command
{
    protected $signature = 'ulauncher:toggle';

    protected $description = 'Toggle launcher.';

    protected string $binary = 'ulauncher-toggle';

    public function handle()
    {
        $command = sprintf(
            'ulauncher-toggle',
            $this->base(),
        );


        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        $this->raw = Str::of(
            shell_exec($command)
        );
    }
}
