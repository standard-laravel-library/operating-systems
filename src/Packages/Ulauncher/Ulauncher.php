<?php

namespace OperatingSystems\Packages\Ulauncher;

use OperatingSystems\Packages\Package;

class Ulauncher extends Package
{
    protected string $name = 'Ulauncher';

    protected ?string $command = 'ulauncher';
}
