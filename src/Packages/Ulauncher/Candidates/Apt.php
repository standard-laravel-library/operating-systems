<?php

namespace OperatingSystems\Packages\Ulauncher\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'ulauncher';

    protected ?string $repository = 'agornostal/ulauncher';
}
