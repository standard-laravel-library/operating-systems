<?php

namespace OperatingSystems\Packages\Ulauncher\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'ulauncher';
}
