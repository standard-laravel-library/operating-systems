<?php

namespace OperatingSystems\Packages\UsbCreatorGtk\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'usb-creator-gtk';
}
