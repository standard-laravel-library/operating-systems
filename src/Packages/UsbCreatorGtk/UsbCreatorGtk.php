<?php

namespace OperatingSystems\Packages\UsbCreatorGtk;

use OperatingSystems\Packages\Package;

class UsbCreatorGtk extends Package
{
    protected string $name = 'USB Creator (GTK)';

    protected ?string $command = '';
}
