<?php

namespace OperatingSystems\Packages\Make\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'make';
}
