<?php

namespace OperatingSystems\Packages\Make\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'make';
}
