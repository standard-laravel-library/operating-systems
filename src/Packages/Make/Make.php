<?php

namespace OperatingSystems\Packages\Make;

use OperatingSystems\Packages\Package;

class Make extends Package
{
    protected string $name = 'Make';

    protected ?string $command = 'make';
}
