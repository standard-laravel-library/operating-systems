<?php

namespace OperatingSystems\Packages;

use OperatingSystems\Commands\Contracts\Command;
use OperatingSystems\Facades\OperatingSystem;
use OperatingSystems\Packages\Contracts\Repository;

abstract class Manager extends Package implements Contracts\Manager
{
    protected string $installer;

    protected function installer(): Command
    {
        return new $this->installer;
    }

    public function isDefault(): bool
    {
        return is_a(OperatingSystem::defaultPackageManager(), static::class);
    }

    public function install(Contracts\Package $package): bool
    {
        // This concept may not be ideal in the end, might not make sense for these classes
        // to have these actions as they require sudo privileges. However, we might also
        // we may also be able to add instructions to add current user to the sudo
        // user list which then would allow these to happen in the application
        return true;
    }

    public function update(Contracts\Package $package): bool
    {
        // This concept may not be ideal in the end, might not make sense for these classes
        // to have these actions as they require sudo privileges. However, we might also
        // we may also be able to add instructions to add current user to the sudo
        // user list which then would allow these to happen in the application
        return true;
    }

    public function uninstall(Contracts\Package $package): bool
    {
        // This concept may not be ideal in the end, might not make sense for these classes
        // to have these actions as they require sudo privileges. However, we might also
        // we may also be able to add instructions to add current user to the sudo
        // user list which then would allow these to happen in the application
        return true;
    }

    protected function installed(Contracts\Package $package): bool
    {
        // This concept may not be ideal in the end as well
        return false;
    }

    protected function notInstalled(Contracts\Package $package): bool
    {
        // This concept may not be ideal in the end as well
        return ! $this->installed($package);
    }

    final public function defaultRepository(): Repository
    {
        return resolve($this->defaultRepository);
    }
}
