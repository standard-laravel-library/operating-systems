<?php

namespace OperatingSystems\Packages\GnomeSoftware;

use OperatingSystems\Packages\Package;

class GnomeSoftware extends Package
{
    protected string $name = 'Gnome Software';

    protected ?string $command = '';
}
