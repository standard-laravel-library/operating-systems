<?php

namespace OperatingSystems\Packages\GnomeSoftware\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'gnome-software';
}
