<?php

namespace OperatingSystems\Packages\GnomeSoftware\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'gnome-software';
}
