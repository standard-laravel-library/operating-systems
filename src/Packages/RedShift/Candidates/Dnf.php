<?php

namespace OperatingSystems\Packages\RedShift\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'redshift';
}
