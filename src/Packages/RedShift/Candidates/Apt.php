<?php

namespace OperatingSystems\Packages\RedShift\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'redshift';
}
