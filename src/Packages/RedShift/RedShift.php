<?php

namespace OperatingSystems\Packages\RedShift;

use OperatingSystems\Packages\Package;

class RedShift extends Package
{
    protected string $name = 'RedShift';

    protected ?string $command = 'redshift';
}
