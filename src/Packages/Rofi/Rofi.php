<?php

namespace OperatingSystems\Packages\Rofi;

use OperatingSystems\Packages\Package;

class Rofi extends Package
{
    protected string $name = 'Rofi';

    protected ?string $command = 'rofi';
}
