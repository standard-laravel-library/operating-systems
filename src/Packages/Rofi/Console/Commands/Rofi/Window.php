<?php

namespace OperatingSystems\Packages\Rofi\Console\Commands\Rofi;

use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Rofi\Console\Commands\Rofi;

class Window extends Command
{
    protected $signature = 'rofi:drun
                            { --N|no-lazy-grab : Disables lazy grab, this forces the keyboard being grabbed before gui is shown. }
    ';

    protected $description = 'An application launcher.';

    public function handle()
    {
        $this->call(
            Rofi::class,
            collect($this->options())->keyBy(fn ($value, $key) => '--'.$key)->put('-s', 'drun')->merge($this->arguments())->toArray()
        );
    }
}
