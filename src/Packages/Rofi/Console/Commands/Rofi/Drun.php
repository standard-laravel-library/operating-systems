<?php

namespace OperatingSystems\Packages\Rofi\Console\Commands\Rofi;

use OperatingSystems\Commands\Command;
use OperatingSystems\Packages\Rofi\Console\Commands\Rofi;

class Drun extends Command
{
    protected $signature = 'rofi:window
                            { --N|no-lazy-grab : Disables lazy grab, this forces the keyboard being grabbed before gui is shown. }
    ';

    protected $description = 'A window switcher.';

    public function handle()
    {
        $this->call(
            Rofi::class,
            collect($this->options())->keyBy(fn ($value, $key) => '--'.$key)->put('-s', 'window')->merge($this->arguments())->toArray()
        );
    }
}
