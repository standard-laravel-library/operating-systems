<?php

namespace OperatingSystems\Packages\Rofi\Console\Commands;

use Illuminate\Support\Str;
use OperatingSystems\Commands\Command;

class Rofi extends Command
{
    protected $signature = 'rofi
                            { --s|show= : Open Rofi in a certain mode. }
                            { --N|no-lazy-grab : Disables lazy grab, this forces the keyboard being grabbed before gui is shown. }
    ';

    protected $description = 'A window switcher, application launcher, ssh dialog, dmenu replacement and more.';

    protected string $binary = 'rofi';

    protected string $optionPrefix = '-';

    protected string $optionSeparator = ' ';

    protected array $forwardableOptions = [
        'show',
        'no-lazy-grab',
    ];

    public function handle()
    {
        $options = $this->prepareForwardableOptions();

        $command = sprintf(
            '%s %s 2>&1',
            $this->base(),
            empty($options) ? '' : ' '.$options,
        );

        if ($this->getOutput()->isDebug()) {
            $this->newLine();
            $this->info('Running shell command: '.$command);
        }

        $this->raw = Str::of(shell_exec($command));
    }
}
