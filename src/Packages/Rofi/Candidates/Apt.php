<?php

namespace OperatingSystems\Packages\Rofi\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'rofi';
}
