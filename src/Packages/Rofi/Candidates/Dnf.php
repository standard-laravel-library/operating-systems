<?php

namespace OperatingSystems\Packages\Rofi\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'rofi';
}
