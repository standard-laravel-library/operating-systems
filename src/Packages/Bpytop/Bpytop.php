<?php

namespace OperatingSystems\Packages\Bpytop;

use OperatingSystems\Packages\Package;

class Bpytop extends Package
{
    protected string $name = 'BPYTOP';

    protected ?string $command = 'bpytop';
}
