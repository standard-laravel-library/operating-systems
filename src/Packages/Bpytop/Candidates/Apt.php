<?php

namespace OperatingSystems\Packages\Bpytop\Candidates;

use OperatingSystems\Packages\Candidates;

class Apt extends Candidates\Apt
{
    protected string $name = 'bpytop';
}
