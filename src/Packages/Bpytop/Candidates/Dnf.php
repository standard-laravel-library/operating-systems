<?php

namespace OperatingSystems\Packages\Bpytop\Candidates;

use OperatingSystems\Packages\Candidates;

class Dnf extends Candidates\Dnf
{
    protected string $name = 'bpytop';
}
