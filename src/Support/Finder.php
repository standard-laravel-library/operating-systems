<?php

namespace OperatingSystems\Support;

use OperatingSystems\Contracts\OperatingSystem;
use OperatingSystems\Fedora;
use OperatingSystems\Kernels\Contracts\Kernel;
use OperatingSystems\Kernels\Linux;
use OperatingSystems\Release;
use OperatingSystems\Ubuntu;

class Finder
{
    public static function detect(Kernel $kernel): ?OperatingSystem
    {
        return match ($kernel::class) {
            Linux::class => static::linux(),
            default => null
        };
    }

    private static function linux(): OperatingSystem
    {
        $release = new Release;

        return match ($release->id) {
            'fedora' => new Fedora($release->id, $release->codename),
            'ubuntu' => new Ubuntu($release->id, $release->codename),
        };
    }
}
