<?php

namespace OperatingSystems\Facades;

use Illuminate\Support\Facades\Facade;
use OperatingSystems\Contracts\OperatingSystem as OperatingSystemContract;

/**
 * @method static \OperatingSystems\Packages\Locators\Contracts\Locator locator()
 * @method static \OperatingSystems\Packages\Managers\Contracts\Manager packageManager()
 */
class OperatingSystem extends Facade
{
    protected static function getFacadeAccessor()
    {
        return OperatingSystemContract::class;
    }
}
