<?php

namespace OperatingSystems;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use OperatingSystems\Commands\Release as Command;

class Release
{
    protected string $id;

    protected string $release;

    protected string $codename;

    public function __construct()
    {
        $raw = $this->raw();

        $this->id = data_get($raw, 'ID');
        $this->release = data_get($raw, 'VERSION_ID');
        $this->codename = data_get($raw, 'VERSION_CODENAME');
    }

    protected function raw(): Collection
    {
        Artisan::call(Command::class, ['-a' => true, '-j' => true]);

        return collect(
            json_decode(
                Artisan::output(),
                true
            )
        );
    }

    final public function toArray(): array
    {
        return [
            'id' => $this->id,
            'release' => $this->release,
            'codename' => $this->codename,
        ];
    }

    final public function __get($property)
    {
        return match ($property) {
            'id', 'release', 'codename' => $this->toArray()[$property],
            default => null
        };
    }
}
